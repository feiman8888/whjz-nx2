package com.whjz.service;

import java.util.Map;

/**
 * 抽象控制器
 */
public interface AbstractController extends Abstract{
    /**
     * 验证请求map里的指定key不能为null（但可以为空字符串）
     */
    void checkNotNull(Map map, String... keys);

    /**
     * 验证请求map里的指定key不能为null或空字符串
     */
    void checkNotNullOrEmpty(Map map, String... keys);
}
