package com.whjz.service.impl;

import com.google.common.base.Preconditions;
import com.whjz.service.AbstractController;
import lombok.extern.slf4j.Slf4j;

import java.util.Map;

@Slf4j
public class AbstractControllerImpl extends AbstractImpl implements AbstractController {

    @Override
    public void checkNotNull(Map map, String... keys) {
        for (String key: keys) {
            Preconditions.checkArgument(map.get(key) != null, "请求参数错误,缺少: "+key);
        }
    }

    @Override
    public void checkNotNullOrEmpty(Map map, String... keys) {
        for (String key: keys) {
            Preconditions.checkArgument(map.get(key) != null && !map.get(key).toString().isEmpty(), "请求参数错误: "+key);
        }
    }
}
