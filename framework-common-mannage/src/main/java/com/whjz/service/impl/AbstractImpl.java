package com.whjz.service.impl;

import com.whjz.service.Abstract;
import com.whjz.utils.ResultUtil;
import com.whjz.utils.Utils;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AbstractImpl implements Abstract {
    @Override
    public Map<String, Object> success(Object data) {
        return ResultUtil.resultMap(null, null, data);
    }

    @Override
    public Map<String, Object> fail(String code, String msg, Object data) {
        return ResultUtil.resultMap(msg, code, data);
    }

    @Override
    public Map page(List allList, int pageNum, int pageSize) {
        List page = Utils.getPage(allList, pageNum, pageSize);

        Map<String, Object> result = new HashMap<>();
        result.put("page", pageNum);
        result.put("pageSize", pageSize);
        result.put("total", allList.size());
        result.put("maxPage", Utils.getTotalPage(allList.size(), pageSize));
        result.put("data", page);
        return result;
    }

    @Override
    public Map page(List pageList, int pageNum, int pageSize, int maxPage, int total) {
        Map<String, Object> result = new HashMap<>();
        result.put("page", pageNum);
        result.put("pageSize", pageSize);
        result.put("total", total);
        result.put("maxPage", maxPage);
        result.put("data", pageList);
        return result;
    }

    @Override
    public void saveCustNo(Map reqBody) {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null) {
            servletRequestAttributes.setAttribute("custNo", reqBody.get("custNo"), ServletRequestAttributes.SCOPE_REQUEST);
        }
    }

    @Override
    public String getCustNo() {
        ServletRequestAttributes servletRequestAttributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (servletRequestAttributes != null) {
            return (String) servletRequestAttributes.getAttribute("custNo", ServletRequestAttributes.SCOPE_REQUEST);
        }else {
            return null;
        }
    }
}
