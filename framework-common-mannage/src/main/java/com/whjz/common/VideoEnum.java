package com.whjz.common;

/**
 * @version 1.0
 * @Description todo
 * @Author lcc
 * @Date 2019/7/19 15:26
 **/
public enum VideoEnum {
    oaVideo("NEWPAD","wh","FILE1015","APP1015"),
    faceVideo("NEWPAD","wh","FILE1014","APP1014");
    VideoEnum(String userName,String pwd,String fileName,String modelCode){
        this.userName = userName;
        this.pwd = pwd;
        this.fileName = fileName;
        this.modelCode = modelCode;
    }

    private String userName;
    private String pwd;
    private String fileName;
    private String modelCode;

    public String getUserName() {
        return userName;
    }

    public String getPwd() {
        return pwd;
    }

    public String getFileName() {
        return fileName;
    }

    public String getModelCode() {
        return modelCode;
    }
}
