package com.whjz.common;

import com.whjz.entity.ICommonAO;
import com.whjz.entity.IParamDTO;

import java.util.Map;

/**
 * 表示需要进行填充
 */
public interface IFill<F extends IParamDTO, T extends ICommonAO> {
    /**
     * 填充
     * @param param 请求变量,可能为null
     * @param ao 响应AO实例
     * @param extra 额外变量,不为null
     */
    void fill(F param, T ao, Map extra);
}
