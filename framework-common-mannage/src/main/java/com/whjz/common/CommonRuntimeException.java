package com.whjz.common;

/**
 * 通用运行时异常
 */
public class CommonRuntimeException extends RuntimeException{
    public CommonRuntimeException() {
        this(ErrorEnum.Error);
    }

    public CommonRuntimeException(ErrorEnum errorEnum) {
        super(errorEnum.getMsg());
    }
}
