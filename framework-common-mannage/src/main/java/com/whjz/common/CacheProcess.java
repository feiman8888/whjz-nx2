package com.whjz.common;

/**
 * 缓存流程
 * @param <T> 结果类
 */
public interface CacheProcess<T> {
    /**
     * 从缓存获取
     * @return 是否有缓存
     */
    boolean getFromCache();

    /**
     * 保存到缓存
     */
    void saveToCache();

    /**
     * 加载数据
     * @return 是否加载成功
     */
    boolean load();

    /**
     * (读取/加载)成功后执行
     * @return 结果,null表示失败
     */
    T onSuccess();

    /**
     * 抛出异常后执行
     * 子类可覆盖
     */
    default void onException(Exception e) {
    }

    /**
     * 开始
     * @return 结果,null表示失败
     */
    default T start() {
        try {
            if (getFromCache()) {//从缓存或者数据库里去去
                //执行
                return onSuccess();
            }else {
                if (load()) {//当上面信息不存在的时候,可以从第三方或者哪里获取数据
                    //设置缓存
                    saveToCache();//塞到缓存或者数据库
                    //执行
                    return onSuccess();
                }else {
                    return null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            onException(e);//异常的时候的处理
            return null;
        }
    }
}
