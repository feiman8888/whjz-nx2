package com.whjz.annotation;


import com.whjz.common.IConverter;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 转换器
 *
 * 可以放在ICommonAO子类内,会在解析ICommonAO子类的实例时解析
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Converter {
    Class<? extends IConverter> value();

    /**
     * 参数列表,默认无
     */
    String[] args() default {};
}
