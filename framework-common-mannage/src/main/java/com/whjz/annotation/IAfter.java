package com.whjz.annotation;

/**
 * ICommonAO的子类及其内的成员变量类(或其任意级子类)实现此接口,会在解析后自动被调用
 */
public interface IAfter {
    /**
     * 在整个AO解析后执行
     */
    void onAfter();
}
