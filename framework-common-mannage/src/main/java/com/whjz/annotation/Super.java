package com.whjz.annotation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 取父值
 *
 * 可以放在ICommonAO子类内,会在解析ICommonAO子类的实例时解析
 */
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Super {
    /**
     * 往上几级,默认1
     * @return >=1
     */
    int level() default 1;

    /**
     * 取哪个成员,默认为空字符串表示取同名成员
     */
    String field() default "";
}
