package com.whjz.annotation;


import com.whjz.entity.ICommonAO;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 放在实现了IFill接口的服务类上
 * 标记此类是填充的服务类
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Fill {
    Class<? extends ICommonAO> value();
}
