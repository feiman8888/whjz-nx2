package com.whjz.utils;

public class RedisKeys {
    public static final String KEY_SPECIFIC_LIST = "specificList_{userId}";

    public static String getKeySpecificList(String userId) {
        return ParamUtil.convert(KEY_SPECIFIC_LIST, Utils.newMap("userId", userId), false);
    }
}
