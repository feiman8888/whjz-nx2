package com.whjz.utils;

import java.text.DecimalFormat;
import java.util.Random;
import java.util.regex.Pattern;

public class CoreConstants {
    public static final Random RANDOM = new Random();

    ////////////////////////
    ///////   时间   ///////
    ////////////////////////
    public static final long SECONDS = 1000;
    public static final long MINUTE = SECONDS*60;
    public static final long HOUR = MINUTE*60;
    public static final long DAY = HOUR*24;

    //performance
    public static final DecimalFormat FORMAT_2 = new DecimalFormat("#.00");

    /**
     * 通用的允许的字符(包含大小写字母与数字)
     * (适合随机生成字符串使用)
     */
    public static final String CHARS = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
    /**
     * 通用的允许的字符除去容易看混淆的一些字符(包含大小写字母与数字)
     * (适合验证码使用)
     */
    public static final String CHARS_SEE = "ABCDEFGHJKMNPQRSTUVWXYZabcdefghjkmnpqrstuvwxyz23456789";

    /**
     * 通用的允许的字符（仅包含数字）
     * (适合验证码使用)
     */
    public static final String CHARS_NUM = "0123456789";

    /**
     * 通用名正则
     * 1. 只能包含字母,数字,下划线,且必须以字母或下划线开头
     * 2. 长度在3-15字符之间
     */
    public static final String NAME_REGEX = "^[a-zA-Z_][a-zA-Z0-9_]{2,14}$";
    /**
     * @see #NAME_REGEX
     */
    public static final Pattern NAME_PATTERN = Pattern.compile(NAME_REGEX);

    /**
     * 邮箱正则
     */
    public static final String MAIL_REGEX = "^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,6}$";
    /**
     * @see #MAIL_REGEX
     */
    public static final Pattern MAIL_PATTERN = Pattern.compile(MAIL_REGEX);

    /**
     * 变量正则
     */
    public static final Pattern PARAMS_PATTERN = Pattern.compile("\\{([\\w]+)\\}");
}
