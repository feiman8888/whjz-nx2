package com.whjz.entity.system;

import lombok.Data;

/**
 * author: fln
 * date: 2020-09-25
 * remarks：配置实体类
 */

@Data
public class SysConfig {
    private Integer autoId;     //自增Id
    private String configCode;  //配置编号
    private String configName;  //配置名称
    private String configValue; //配置值
    private String remark;  //备注
    private String state;  //状态 1-启用，2--停用
    private String delFlag;     //删除标识
    private String createDate;  //创建时间
    private String updateDate;  //更新时间
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
}
