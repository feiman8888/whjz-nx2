package com.whjz.entity.system;


import lombok.Data;

/**
 * author: fln
 * date: 2020-09-24
 * remarks 日志实体类
 */

@Data
public class SysRequestLog {
    private Integer autoId; //自增Id
    private String createDate;  //创建时间
    private String updateDate;  //修改时间
    private String channel; //渠道（01普通、02语音、03视频）
    private String consumerSeqNo;   //前端流水
    private String backSeqNo;   //后台流水号
    private String imageSeqNo;  //影像流水
    private String interfaceInfo;   //接口信息
    private String operateUser; //操作用户
    private String requestParameters;   //请求参数
    private String requestSource;   //请求来源
    private String requestUrl;  //请求url
    private String responseResult;  //返回结果
    private String responseState;   //返回状态
    private String time;    //请求时长
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
}
