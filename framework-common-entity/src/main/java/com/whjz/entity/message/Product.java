package com.whjz.entity.message;

import lombok.Data;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：产品实体类
 */

@Data
public class Product {
    private Integer autoId;     //自增ID
    private String productCode;   //产品编号
    private String productName;   //产品名称
    private String productType;   //产品分类 三级联动
    private String productBusinessId;    //商家id
    private String productBusinessName;   //商家名称
    private String productSpecifications;   //规格
    private String platformPrice;    //平台价格
    private String supermarketPrice;   //超市价格
    private String crazyRequisitionDate;  //疯狂请购时间
    private String salesVolume;  //销售量
    private String productDetails;   //商品详情
    private String createUser; //录入人
    private String createDate; //录入时间
    private String ext1;    //扩展字段1
    private String ext2;    //扩展字段2
    private String ext3;    //扩展字段3
    private String ext4;    //扩展字段4
    private String ext5;    //扩展字段5
}
