package com.whjz.entity.message;

import lombok.Data;
/**
* author:hj
* date:2020-10-6
* remarks:供应商联系人管实体类
*/
@Data
public class SupplierContacts{
    /**
     * 序号
     */
    private Integer autoId;
    /**
     * 供应商编码
     */
    private  String supplierCode;
    /**
     * 供应商联系人
     */
    private String supplierName;
    /**
     * 联系人电话
     */
    private  String supplierTel;
    /**
     * 联系人微信
     */
    private  String supplierWeChat;
    /**
     *创建人
     *
     */
    private  String createUser;
    /**
     * 创建时间
     */
    private  String createDate;

    private String ext1;            //扩展字段1
    private String ext2;            //扩展字段2
    private String ext3;            //扩展字段3
    private String ext4;            //扩展字段4
    private String ext5;            //扩展字段5

}

