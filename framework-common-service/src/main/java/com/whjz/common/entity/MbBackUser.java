package com.whjz.common.entity;

import java.math.BigDecimal;

public class MbBackUser {
    private BigDecimal autoId;

    private String createDate;

    private String updateDate;

    private String userPasswd;

    private String userPasswdUpdate;

    private String vuserId;

    private String vuserOrgid;

    private String operator;

    private String phone;

    private String userId;

    private String userIdKey;

    private String userIdNumber;

    private String userName;

    private String userState;

    private String orgId;

    private String ext1;

    private String ext2;

    private String ext3;

    private String ext4;

    private String ext5;
    private String token;

    private String orgName;

    private String userSex;

    private String vchrboxno;

    private String orgLevel;

    public String getOrgLevel() {
        return orgLevel;
    }

    public void setOrgLevel(String orgLevel) {
        this.orgLevel = orgLevel;
    }

    public BigDecimal getAutoId() {
        return autoId;
    }

    public void setAutoId(BigDecimal autoId) {
        this.autoId = autoId;
    }

    public String getCreateDate() {
        return createDate;
    }

    public void setCreateDate(String createDate) {
        this.createDate = createDate == null ? null : createDate.trim();
    }

    public String getUpdateDate() {
        return updateDate;
    }

    public void setUpdateDate(String updateDate) {
        this.updateDate = updateDate == null ? null : updateDate.trim();
    }

    public String getUserPasswd() {
        return userPasswd;
    }

    public void setUserPasswd(String userPasswd) {
        this.userPasswd = userPasswd == null ? null : userPasswd.trim();
    }

    public String getUserPasswdUpdate() {
        return userPasswdUpdate;
    }

    public void setUserPasswdUpdate(String userPasswdUpdate) {
        this.userPasswdUpdate = userPasswdUpdate == null ? null : userPasswdUpdate.trim();
    }

    public String getVuserId() {
        return vuserId;
    }

    public void setVuserId(String vuserId) {
        this.vuserId = vuserId == null ? null : vuserId.trim();
    }

    public String getVuserOrgid() {
        return vuserOrgid;
    }

    public void setVuserOrgid(String vuserOrgid) {
        this.vuserOrgid = vuserOrgid == null ? null : vuserOrgid.trim();
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator == null ? null : operator.trim();
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone == null ? null : phone.trim();
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId == null ? null : userId.trim();
    }

    public String getUserIdKey() {
        return userIdKey;
    }

    public void setUserIdKey(String userIdKey) {
        this.userIdKey = userIdKey == null ? null : userIdKey.trim();
    }

    public String getUserIdNumber() {
        return userIdNumber;
    }

    public void setUserIdNumber(String userIdNumber) {
        this.userIdNumber = userIdNumber == null ? null : userIdNumber.trim();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName == null ? null : userName.trim();
    }

    public String getUserState() {
        return userState;
    }

    public void setUserState(String userState) {
        this.userState = userState == null ? null : userState.trim();
    }

    public String getOrgId() {
        return orgId;
    }

    public void setOrgId(String orgId) {
        this.orgId = orgId == null ? null : orgId.trim();
    }

    public String getExt1() {
        return ext1;
    }

    public void setExt1(String ext1) {
        this.ext1 = ext1 == null ? null : ext1.trim();
    }

    public String getExt2() {
        return ext2;
    }

    public void setExt2(String ext2) {
        this.ext2 = ext2 == null ? null : ext2.trim();
    }

    public String getExt3() {
        return ext3;
    }

    public void setExt3(String ext3) {
        this.ext3 = ext3 == null ? null : ext3.trim();
    }

    public String getExt4() {
        return ext4;
    }

    public void setExt4(String ext4) {
        this.ext4 = ext4 == null ? null : ext4.trim();
    }

    public String getExt5() {
        return ext5;
    }

    public void setExt5(String ext5) {
        this.ext5 = ext5 == null ? null : ext5.trim();
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName == null ? null : orgName.trim();
    }

    public String getUserSex() {
        return userSex;
    }

    public void setUserSex(String userSex) {
        this.userSex = userSex == null ? null : userSex.trim();
    }

    public String getVchrboxno() {
        return vchrboxno;
    }

    public void setVchrboxno(String vchrboxno) {
        this.vchrboxno = vchrboxno == null ? null : vchrboxno.trim();
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}