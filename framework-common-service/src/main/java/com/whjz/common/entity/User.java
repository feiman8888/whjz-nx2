package com.whjz.common.entity;

import java.io.Serializable;

import javax.persistence.Column;

import com.whjz.entity.BaseModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class User extends BaseModel implements Serializable {

	private static final long serialVersionUID = 1448226115712908057L;

	@ApiModelProperty(value = "用户唯一标识")
	@Column(name = "user_id", columnDefinition = "varchar(50)  NOT NULL COMMENT '用户唯一标识'")
	private String userId;

	@ApiModelProperty(value = "姓名")
	@Column(name = "user_name", columnDefinition = "varchar(50)  NOT NULL COMMENT '姓名'")
	private String userName;

	@ApiModelProperty(value = "密码")
	@Column(name = "password", columnDefinition = "varchar(50)  NOT NULL COMMENT '密码'")
	private String password;

	@ApiModelProperty(value = "手机号")
	@Column(name = "phone", columnDefinition = "varchar(50)  DEFAULT NULL COMMENT '手机号'")
	private String phone;

}
