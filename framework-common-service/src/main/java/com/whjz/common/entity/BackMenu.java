package com.whjz.common.entity;

import com.whjz.entity.BackstageMenu;
import lombok.Data;

/**
 * @version 1.0
 * @Description todo
 * @Author lcc
 * @Date 2019/5/31 14:34
 **/
@Data
public class BackMenu extends BackstageMenu{
    private String code;

}
