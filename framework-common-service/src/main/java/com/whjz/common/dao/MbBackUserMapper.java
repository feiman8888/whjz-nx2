package com.whjz.common.dao;

import com.whjz.common.entity.BackMenu;
import com.whjz.entity.MbBackUser;
import com.whjz.entity.MbBankOrg;
import com.whjz.entity.MbBankRole;
import com.whjz.entity.BackstageMenu;

import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface MbBackUserMapper {
    int deleteByPrimaryKey(BigDecimal autoId);

    int insert(MbBackUser record);

    int insertSelective(MbBackUser record);

    MbBackUser selectByPrimaryKey(BigDecimal autoId);

    int updateByPrimaryKeySelective(MbBackUser record);

    int updateByPrimaryKey(MbBackUser record);

//   新增
    MbBackUser selectByUserId(String userId); // 根据用户号查找密码**/

    int updateByUserId(MbBackUser record); //根据用户号修改信息**/

    List<BackstageMenu> queryUserMenu(Map req);//根据用户号查询用户菜单 by czl**/

    List<BackMenu> queryUserLoveMenu(Map req); ///**获取一见倾心菜单**/

    List<MbBankRole> queryUserRole(Map req);

    MbBankOrg selectOrgInfoByOrgCode(String orgCode);//根据机构号查询机构信息
}