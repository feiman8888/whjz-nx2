package com.whjz.common.dao;

import com.whjz.entity.MbUserLocation;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

public interface MbUserLocationMapper {
    int deleteByPrimaryKey(BigDecimal autoId);

    int insert(MbUserLocation record);

    int insertSelective(MbUserLocation record);

    MbUserLocation selectByPrimaryKey(BigDecimal autoId);

    int updateByPrimaryKeySelective(MbUserLocation record);

    int updateByPrimaryKey(MbUserLocation record);

    Integer countList(Map<String, String> req);

    List<MbUserLocation> queryList(Map<String, String> req);

    List<MbUserLocation> queryAllList(Map<String, String> req);
}