package com.whjz.common.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.whjz.common.dao.*;
import com.whjz.common.entity.BackMenu;
import com.whjz.common.dao.*;
import com.whjz.common.entity.DictDTO;
import com.whjz.common.service.CommonService;
import com.whjz.entity.*;
import com.whjz.entity.*;
import com.whjz.redis.RedisUtils;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

import java.util.*;


@Service(value = "commonService")
@Slf4j
@SuppressWarnings({"rawtypes", "unchecked"})
public class CommonServiceImpl implements CommonService {


    @Autowired
    private Environment environment;
    @SuppressWarnings("all")
    @Autowired
    private MbRequestLogMapper mbRequestLogMapper;
    @SuppressWarnings("all")
    @Autowired
    private MbBackUserMapper mbBackUserMapper;

    @Autowired
    @SuppressWarnings("all")
    private IbsaccessLogServiceImpl ibsaccessLogService;
    @Autowired
    @SuppressWarnings("all")
    IbsaccessLogForLoginMapper ibsaccessLogForLoginMapper;

    @Autowired
    @SuppressWarnings("all")
    private MbCommonMapper mbCommonMapper;

    @Autowired
    @SuppressWarnings("all")
    private CommonMapper commonMapper;


    /**
     * 获取配置列表
     */
    @Override
    public Map<String, Object> getListConfig() {
        String config = RedisUtils.getValue("config");        //从redis中获取字典集合
        if (config == null) {
            initConfig();            //重新加载
        }
        config = RedisUtils.getValue("config");        //从redis中获取字典集合
        Map<String, Object> map_config = (Map<String, Object>) JSONObject.parseObject(config);
        return map_config;
    }

    /**
     * 获取字典集合
     */
    @Override
    public Map<String, Object> getListDict() {
        String dict = RedisUtils.getValue("dict");        //从redis中获取字典集合
        if (dict == null) {
            initDict();        //进行字典重新加载
        }
        dict = RedisUtils.getValue("dict");            //从redis中获取字典集合
        Map<String, Object> map_dict = (Map<String, Object>) JSONObject.parseObject(dict);
        return map_dict;
    }

    /**
     * 配置初始化
     */
    public void initConfig() {
        List<Config> list_config = commonMapper.getListConfig();        // 配置集合
        Map<String, String> map_config = new HashMap<String, String>(); // 配置集合对象

        // 配置信息放入静态MAP对象
        for (Config c : list_config) {
            map_config.put(c.getConfigCode(), c.getConfigValue());
        }
        Long lostTime = environment.getProperty("redis.maxInactiveIntervalInSeconds", Long.class);
        RedisUtils.setKeyAndCacheTime("config", JSONObject.toJSONString(map_config), lostTime);        //对配置进行格式化后放入redis
    }

    /**
     * 字典配置初始化
     */
    public void initDict() {
        // 字典集合
        List<DictDTO> list_dict = commonMapper.getListDict();
        // 配置集合
//        List<Config> list_config = commonMapper.getListConfig(); 配置表getListConfig没有用   暂时注释掉
        List<Config> list_config = new ArrayList<>();
        Map<String, Object> map_dict = CacheUtil.getMapDict(); // 字典集合对象
        // 获取字典集合
        Set<String> set_dict_type = new HashSet<String>();
        for (DictDTO d : list_dict) {
            set_dict_type.add(d.getDictTypeCode());
        }

        for (String type : set_dict_type) {
            JSONArray jsonArr = new JSONArray();

            Config defConfig = null;
            // 匹配是否满足配置表中的
            for (Config c : list_config) {
                // 存放这个
                if (c.getConfigCode().equals(type)) {
                    defConfig = c;
                }
            }

            for (DictDTO d : list_dict) {
                // 找到该type下的所有字典集合
                if (type.equals(d.getDictTypeCode())) {
                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("text", d.getDictName());
                    jsonObject.put("dictValue", d.getDictValue());
                    jsonObject.put("defaultFlag", 0);
                    // 匹配设置默认值
                    if (defConfig != null) {
                        if (defConfig.getConfigValue().equals(d.getDictValue())) {
                            jsonObject.put("defaultFlag", 1);
                        }
                    }
                    jsonArr.add(jsonObject);
                }
            }
            map_dict.put(type, jsonArr); // 存放各个字典类型的字典数据
        }

        // 把配置 和字典集合放入redis
        Long lostTime = environment.getProperty("redis.maxInactiveIntervalInSeconds", Long.class);
        RedisUtils.setKeyAndCacheTime("dict", JSONObject.toJSONString(map_dict), lostTime);            //对字典进行格式化后放入redis
    }


    /**
     * @param subordinate 下级
     * @param superior    上级
     */
    public void toDeal(Map<String, Object> subordinate, Map<String, Object> superior) {
        for (Map.Entry<String, Object> entry : superior.entrySet()) {

            List s = (List) entry.getValue();
            List n = new ArrayList();
            Map m = new HashMap();
            for (int i = 0; i < s.size(); i++) {
                m.put(s.get(i), (List) subordinate.get(s.get(i)));
            }
            n.add(m);
            entry.setValue(n);
        }
    }


    /**
     * @return
     * @author ： lcc
     * @DEsc :获取字典项
     * @date: 2019/5/13 11:20
     * @Param: null
     */
    @Override
    public List queryDictByKey(Map<String, String> req) {
        return commonMapper.queryDictByKey(req);
    }


    /**
     * 获取最大后台流水号
     *
     * @param req
     * @return
     */
    @Override
    public String queryTheMaxBackReqNO(Map<String, String> req) {
        com.whjz.common.entity.MbRequestLog mbRequestLog = mbRequestLogMapper.selectTheMaxBackReqNo();
        if (mbRequestLog == null)
            return "";
        return mbRequestLog.getBackSeqNo();
    }

    /**
     * 校验用户密码 规则如下：
     * 1.前段传过来的密码进行二次加密 二次加密之后的密码与数据库的密码进行比较
     * 2.redis集群中将记录密码的错误次数，如果超过 则更新数据库密码开关情况
     * 3.如果超过次数则 返回超过次数，需后管进行维护
     * 4.判断是否超过180天，如超过则返回 必须修改密码
     * 5.重置密码或初始密码时，user_passwd_updatetime 为99999999,
     *
     * @param req
     * @return
     */
    @Override
    public Map<String, Object> checkTheUserPasswd(Map<String, String> req) {
        //2.redis集群中将记录密码的错误次数，如果超过 则更新数据库密码开关情况
        MbBackUser mbBackUser = mbBackUserMapper.selectByUserId(req.get(StringConstantUtil.USER_DEFAULT));
        if (mbBackUser == null) {
            return ResultUtil.resultMap(CodeUtil.CODE_201.getName(), CodeUtil.CODE_201.getCode(), new HashMap());
        }
        String userId = mbBackUser.getUserId();
        String passwd = AppUtils.md5Encode(req.get(StringConstantUtil.USER_PASSWD_FRONT_DEFAULT));//获取前端传过来密码
        Map<String, String> in = new HashMap<>();
        in.put("passwordStatusCode", "200");//密码状态 200为正常

        //针对超级管理员角色 密码只校验对错 start
        Map roleParam = new HashMap();
        roleParam.put("userId", userId);
        List<MbBankRole> userRoleList = mbBackUserMapper.queryUserRole(roleParam);
        if (userRoleList != null && !userRoleList.isEmpty()) {
            MbBankRole userRole = userRoleList.get(0);
            if ("R0001".equals(StringUtils.getString(userRole.getRoleId()))) {
                if (mbBackUser.getUserPasswd().equals(passwd)) {
                    return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), new HashMap());
                } else {
                    return ResultUtil.resultMap(CodeUtil.CODE_203.getName(), CodeUtil.CODE_203.getCode(), new HashMap());
                }
            }
        }
        //针对超级管理员角色 密码只校验对错 end

        String errorCount = RedisUtils.getValue(StringConstantUtil.USER_PASSWD_ERROR_COUNT + userId);
        if (StringUtils.isNotNullOrEmtory(errorCount)) {
            if (StringConstantUtil.USER_PASSWD_ERROR_COUNT_EN < Integer.parseInt(errorCount)) {
                //3.如果超过次数则 返回超过次数，需后管进行维护
                return ResultUtil.resultMap(CodeUtil.CODE_202.getName(), CodeUtil.CODE_202.getCode(), new HashMap());
            }
        }

        //1.前段传过来的密码进行二次加密 二次加密之后的密码与数据库的密码进行比较
        if (mbBackUser.getUserPasswd().equals(passwd)) {
            RedisUtils.setKey(StringConstantUtil.USER_PASSWD_ERROR_COUNT + userId, "0");
            if (StringConstantUtil.USER_PASSWD_UPDATETIME.equals(mbBackUser.getUserPasswdUpdate())) {
                in.put("passwordStatusCode", CodeUtil.CODE_205.getCode());
                return ResultUtil.resultMap(CodeUtil.CODE_205.getName(), CodeUtil.CODE_200.getCode(), in);
            }
            //4.判断是否超过180天，如超过则返回 必须修改密码
            Long toDate = DateUtils.getQuotByDaysyyyyMMdd(mbBackUser.getUserPasswdUpdate(), DateUtils.getyyyyMMddCurDate());
            if (toDate.longValue() > (new Long(StringConstantUtil.USER_PASSWD_MAX_UN))) {
                in.put("passwordStatusCode", CodeUtil.CODE_204.getCode());
                return ResultUtil.resultMap(CodeUtil.CODE_204.getName(), CodeUtil.CODE_200.getCode(), in);
            }

            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), new HashMap());
        }
        //提示密码错误次数
        String nowErrorCount = AmountUtil.add(errorCount, "1", 0);
        RedisUtils.setKey(StringConstantUtil.USER_PASSWD_ERROR_COUNT + userId, nowErrorCount);
        Integer remainCount = StringConstantUtil.USER_PASSWD_ERROR_COUNT_EN + 1 - Integer.parseInt(nowErrorCount);
        return ResultUtil.resultMap(CodeUtil.CODE_203.getName() + ",还剩" + remainCount + "次机会", CodeUtil.CODE_203.getCode(), new HashMap());
    }

    /**
     * 修改用户密码，规则如下
     * 1.redis集群中 USER_PASSWD_ERROR_COUNT置为0
     * 2.数据库中密码置为 上送数据（二次加密），密码修改时间置为今天
     *
     * @param req
     * @return
     */
    @Override
    public Map<String, Object> resetPasswd(Map<String, String> req) {
        //2.数据库中密码置为 上送数据（二次加密），密码修改时间置为今天
        String userId = StringUtils.getString(req.get(StringConstantUtil.USER_DEFAULT));
        String newUserPasswd = AppUtils.md5Encode(StringUtils.getString(req.get(StringConstantUtil.USER_PASSWD_FRONT_DEFAULT)));
        MbBackUser mbBackUserExcit = mbBackUserMapper.selectByUserId(userId);
        if (mbBackUserExcit != null) {
            String oldUserPasswd = mbBackUserExcit.getUserPasswd();
            if (oldUserPasswd.equals(newUserPasswd)) {
                return ResultUtil.resultMap(CodeUtil.CODE_10035.getName(), CodeUtil.CODE_10035.getCode(), new HashMap());
            } else {
                MbBackUser mbBackUser = new MbBackUser();
                mbBackUser.setUserPasswd(newUserPasswd);
                mbBackUser.setExt1(CodeUtil.PasswodStatus_Change.getCode());//设置密码状态为 修改后的状态
                mbBackUser.setUserPasswdUpdate(DateUtils.getCurDate(DateUtils.yyyyMMdd_EN));
                mbBackUser.setUserId(req.get(StringConstantUtil.USER_DEFAULT));
                return updateUser(mbBackUser);
            }
        }
        return ResultUtil.resultMap(CodeUtil.CODE_10033.getName(), CodeUtil.CODE_10033.getCode(), new HashMap());
    }

    /**
     * 设置默认密码 规则如下：
     * 1.redis集群中 USER_PASSWD_ERROR_COUNT置为0
     * 2.数据库中 密码置为USER_PASSWD_DEFAULT 密码修改时间置为 USER_PASSWD_UPDATETIME
     *
     * @param req
     * @return
     */
    @Override
    public Map<String, Object> resetDefaultPasswd(Map<String, String> req) {
        //2.数据库中 密码置为 USER_PASSWD_DEFAULT 密码修改时间置为 USER_PASSWD_UPDATETIME
        String custNo = StringUtils.getString(req.get(StringConstantUtil.USER_DEFAULT));
        String userId = StringUtils.getString(req.get("userId"));
        String sourceType = StringUtils.getString(req.get(StringConstantUtil.USER_SOURCETYPE_DEFAULT));
        String phoneEightPassw = StringUtils.getString(req.get("phoneEightPassw"));//前端传加密后的字符串
        MbBackUser mbBackUserExcit = mbBackUserMapper.selectByUserId(userId);
        MbBackUser mbBackUserUpdate = new MbBackUser();//要更新的用户信息
        if (mbBackUserExcit != null) {
            mbBackUserUpdate.setUserPasswd(AppUtils.md5Encode(phoneEightPassw));//获取手机号后6位密码
            mbBackUserUpdate.setExt1(CodeUtil.PasswodStatus_Reset.getCode());//设置密码状态为 重置后默认密码状态
            mbBackUserUpdate.setUserPasswdUpdate(DateUtils.getCurDate(DateUtils.yyyyMMdd_EN));
            mbBackUserUpdate.setOperator(custNo);//操作人员
            mbBackUserUpdate.setUserId(userId);//更新密码的柜员号

            //mbBackUserUpdate.setExt1(CodeUtil.PasswodStatus_Change.getCode());//设置密码状态为 重置后默认密码状态
//           mbBackUserUpdate.setUserPasswd(StringConstantUtil.USER_PASSWD_DEFAULT); //这是设置密码为111 已不用（测试用这个）
            return updateUser(mbBackUserUpdate);
        } else {
            return ResultUtil.resultMap(CodeUtil.CODE_10011.getName(), CodeUtil.CODE_10011.getCode(), new HashMap());
        }


    }

    /**
     * @Desc :密码重置为默认密码
     * @Author :  @Date: 2019/10/22 16:11
     * @Param: @Return:
     */
    private Map<String, Object> updateUser(MbBackUser mbBackUser) {
        int res = mbBackUserMapper.updateByUserId(mbBackUser);
        if (res > 0) {
            //1.redis集群中 USER_PASSWD_ERROR_COUNT置为0
            RedisUtils.setKey(StringConstantUtil.USER_PASSWD_ERROR_COUNT + mbBackUser.getUserId(), "0");
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), new HashMap());
        }
        return ResultUtil.resultMap(CodeUtil.CODE_207.getName(), CodeUtil.CODE_207.getCode(), new HashMap());
    }


    /**
     * 公共登录，规则如下：
     * 1.根据sourceType 来区分后管登录与pad登录
     * 2.将进行密码校验
     * 3.插入登录信息表
     * 4.更新redis集群 token信息
     * 5.查询菜单
     */
    @Override
    public Map<String, Object> login(Map<String, String> reqBody) {
        log.info("密码登录请求参数：" + JSONObject.toJSON(reqBody));
        String userId = reqBody.get(StringConstantUtil.USER_DEFAULT);// 行员工号
        String password = reqBody.get(StringConstantUtil.USER_PASSWD_FRONT_DEFAULT);//密码
        String deviceId = reqBody.get(StringConstantUtil.USER_DEVICEID_DEFAULT);//设备号

        String sourceType = reqBody.get(StringConstantUtil.USER_SOURCETYPE_DEFAULT);// 1-app 2-后管
        String ip = reqBody.get(StringConstantUtil.USER_IP_DEFAULT);// 2-app 3-后管
        if (org.apache.commons.lang.StringUtils.isBlank(sourceType)) {
            sourceType = "2";//移动工作站登录
        }
        if (org.apache.commons.lang.StringUtils.isBlank(userId)) {
            return ResultUtil.resultMap("请输入账号", CodeUtil.CODE_10000.getCode(), null);
        }
        if (org.apache.commons.lang.StringUtils.isBlank(password)) {
            return ResultUtil.resultMap("请输入密码", CodeUtil.CODE_10000.getCode(), null);
        }
        // 查询客户号信息
        log.info("用户登录请求：统一认证验证开始 ...");
        String manageLogin = "";
        if (reqBody.containsKey("manageLogin")) {
            manageLogin = StringUtils.getString(reqBody.get("manageLogin"));
        }
        if (!"noCheck".equals(manageLogin)) {//后管定时任务不短信验证
            //短信验证码校验
            if (StringConstantUtil.USER_SOURCETYPE_APP.equals(sourceType)) {
                String uuid = StringUtils.getString(reqBody.get("uuid"));
                String randNo = StringUtils.getString(reqBody.get("noteNo"));
                String uuidUserId = uuid + userId;
                String redisRandNo = RedisUtils.getValue(uuidUserId);
                if (StringUtils.isNotNullOrEmtory(redisRandNo)) note:{
                    if (redisRandNo.equals(randNo)) {
                        break note;
                    } else {
                        return ResultUtil.resultMap("验证码输入错误", ResultCode.RESUTL_CODE_FAILURE.code(), new HashMap<>());
                    }
                }
                else {
                    return ResultUtil.resultMap("验证码已失效,请重新发送短信", ResultCode.RESUTL_CODE_FAILURE.code(), new HashMap<>());
                }
            }
        }

        Map<String, Object> loginResult = null;
        try {
            if ("noCheck".equals(manageLogin)) {
                loginResult = new HashMap<>();
                loginResult.put("code", CodeUtil.CODE_200.getCode());
                Map dataMap = new HashMap();
                loginResult.put("data", dataMap);
            } else {
                //2.将进行密码校验
                loginResult = checkTheUserPasswd(reqBody);
            }

        } catch (Exception e) {
            log.info("用户登录请求：统一认证平台验证异常 ...[{}]", e);
            return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
        }
        //1.根据sourceType 来区分后管登录与pad登录
        reqBody.put("ext1", sourceType);
        reqBody.put("loginResult", StringUtils.getString(loginResult.get("code")));
        reqBody.put("ext2", CodeUtil.TRADITION_LOGIN.getCode());
        reqBody.put("ext4", ip);
        reqBody.put("createdate", DateUtils.getCurDateTime());
        reqBody.put("updatedate", DateUtils.getCurDateTime());
        if (!CodeUtil.CODE_200.getCode().equals(loginResult.get("code"))) {
            // 失败的情况下 客户信息赋值
            // 添加日志
            //3.插入登录信息表
            ibsaccessLogService.insertIbsaccessLog(reqBody);

            return loginResult;
        }
        log.info("登录返回的信息:" + JSONObject.toJSONString(loginResult));

        // 登录成功之后，之后进行token,等信息保存
        String token = TokenUtil.getGeneratePassword();
        loginResult.put("token", token);
        //4.更新redis集群 token信息
        MbBackUser mbBackUser = mbBackUserMapper.selectByUserId(userId);
        mbBackUser.setAutoId(null);//设置返回autoId为空  否则会baseInfo里面传过来不好
        //获取柜员机构信息
        String orgCode = StringUtils.getString(mbBackUser.getOrgId());
        MbBankOrg mbBankOrg = mbBackUserMapper.selectOrgInfoByOrgCode(orgCode);
        if (mbBankOrg != null) {
            mbBackUser.setOrgName(mbBankOrg.getOrgName());
            mbBackUser.setOrgLevel(StringUtils.getString(mbBankOrg.getOrgLevel()));
        }
        Long redisLostTime = environment.getProperty("redis.maxInactiveIntervalInSeconds", Long.class);
        if (StringConstantUtil.USER_SOURCETYPE_APP.equals(sourceType)) {
            RedisUtils.setKeyAndCacheTime(StringConstantUtil.TOKEN + userId, token, redisLostTime);
            RedisUtils.setKeyAndCacheTime(StringConstantUtil.USER + userId, JSONObject.toJSONString(mbBackUser), redisLostTime);
        } else {
            RedisUtils.setKeyAndCacheTime(StringConstantUtil.MANAGE_TOKEN_ASSIST + userId, token, redisLostTime);
            RedisUtils.setKeyAndCacheTime(StringConstantUtil.MANAGE_USER_ASSIST + userId, JSONObject.toJSONString(mbBackUser), redisLostTime);
        }
        Map m = (Map) loginResult.get("data");
        //内管返回结果data里面放置登录信息
        if (!StringConstantUtil.APPSOURCETYPE.equals(sourceType)) {
            List menu = new ArrayList();
            mbBackUser.setUserPasswd("");
            mbBackUser.setToken(token);
            m.put("loginInfo", mbBackUser);
            //查询菜单树
            menu = getLoginMenu(userId, sourceType);
            //如果是app 则返回一见倾心
            if (StringConstantUtil.USER_SOURCETYPE_APP.equals(sourceType)) {
                m.put("loveMenu", getLoginLoveMenu(userId, sourceType));
            }
            m.put("menuList", menu);
        }
        //内管查询用户关联的角色
        if (!StringConstantUtil.APPSOURCETYPE.equals(sourceType)) {
            Map userRoleMap = new HashMap();
            userRoleMap.put("userId", userId);
            List<MbBankRole> userRole = mbBackUserMapper.queryUserRole(userRoleMap);
            m.put("userRoleList", userRole);
        }

        //如果是默认密码 重置后的密码也提示 让柜员跳转到修改密码页面
        String passwordStatus = StringUtils.getString(mbBackUser.getExt1());
        if (CodeUtil.PasswodStatus_Reset.getCode().equals(passwordStatus)) {
            m.put("loginState", CodeUtil.Login_Status_FristLogin.getCode());
        }
        //查询是否是首次登录

        //内管如果密码超过180天未修改  跳转到密码修改页面
        if (!StringConstantUtil.APPSOURCETYPE.equals(sourceType)) {
            if (m.containsKey("passwordStatusCode")) {
                String passwordStatusCode = StringUtils.getString(m.get("passwordStatusCode"));
                if (CodeUtil.CODE_204.getCode().equals(passwordStatusCode)) {
                    m.put("loginState", CodeUtil.CODE_204.getCode());
                }
            }
        }
        loginResult.put("data", m);
        reqBody.put("ext3", token);
        //3.插入登录信息表
        ibsaccessLogService.insertIbsaccessLog(reqBody);
        log.info("登陆验证结束 登陆成功...");
        log.info("密码登录返回结果：" + JSONObject.toJSON(loginResult));
        return loginResult;
    }

    /**
     * @Desc :根据用户编号查询用户信息
     * @Author :gzx    @Date: 2019/10/18 20:41
     * @Param: @Return:
     */
    @Override
    public Map<String, Object> selectUserInfoByUserId(Map<String, String> req) {
        try {
            String userId = StringUtils.getString(req.get("userId"));
            MbBackUser mbBackUser = mbBackUserMapper.selectByUserId(userId);
            return ResultUtil.resultMap("查询成功", ResultCode.RESULT_CODE_SUCCESS.code(), mbBackUser);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap("查询失败", ResultCode.RESUTL_CODE_FAILURE.code(), null);
    }

    /**
     * @return
     * @author ： czl
     * @DEsc : 获取登录倾馨菜单
     * @date: 2019/8/31 15:21
     * @Param: null
     */
    public Map getLoginLoveMenu(String userId, String sourceType) {
        Map reqm = getReq(userId, sourceType);
        try {
            List<BackMenu> list = mbBackUserMapper.queryUserLoveMenu(reqm);
            return buildLoveMap(list);
        } catch (Exception e) {
            return new HashMap();
        }
    }

    private Map getReq(String userId, String sourceType) {
        Map<String, String> in = new HashMap<>();
        if (StringConstantUtil.USER_SOURCETYPE_APP.equals(sourceType)) {
            in.put("source", "1");
        }
        if (StringConstantUtil.USER_SOURCETYPE_MANAGE.equals(sourceType)) {
            in.put("source", "2");
        }
        in.put("userId", userId);
        return in;
    }

    /**
     * @return
     * @author ： lcc
     * @DEsc :生成倾馨菜单
     * @date: 2019/5/31 15:40
     * @Param: null
     */
    public Map buildLoveMap(List<BackMenu> list) {
        Map map = new HashMap();
        for (BackMenu m : list) {
            String code = m.getCode();
            List<BackMenu> bList = new ArrayList<>();
            if (map.get(code) != null) {
                bList = (List<BackMenu>) map.get(m.getCode());
            }
            bList.add(m);
            map.put(code, bList);
        }
        return map;
    }

    /**
     * 查询菜单树，根据sourceType 2-pad的 3-后管
     *
     * @param userId
     * @param sourceType
     * @return
     */
    private List getLoginMenu(String userId, String sourceType) {
        Map reqm = getReq(userId, sourceType);
        List<BackstageMenu> menus = mbBackUserMapper.queryUserMenu(reqm);
        List<BackstageMenu> list = buildMenuTree(menus);
        return list;
    }

    /**
     * @return
     * @author ： lcc
     * @DEsc :建造菜单树
     * @date: 2019/5/31 14:45
     * @Param: null
     */
    public static List<BackstageMenu> buildMenuTree(List<BackstageMenu> list) {
        List<BackstageMenu> result = new ArrayList<>();
        for (BackstageMenu m : list) {
            if (org.apache.commons.lang.StringUtils.isBlank(m.getParentId())) {
                result.add(findChild(m, list));
            }
        }
        return result;
    }

    /**
     * @return
     * @author ： lcc
     * @DEsc :递归树节点
     * @date: 2019/5/31 14:45
     * @Param: null
     */
    public static BackstageMenu findChild(BackstageMenu menu, List<BackstageMenu> list) {
        if (menu.getChildren() == null) {
            menu.setChildren(new ArrayList<>());
        }
        for (BackstageMenu m : list) {
            if ((menu.getMenuCode()).equals(m.getParentId())) {
                menu.getChildren().add(findChild(m, list));
            }
        }
        return menu;
    }


    /**
     * @Desc :退出登录
     * @Author :gzx    @Date: 2019/10/22 15:33
     * @Param: @Return:
     */
    @Override
    public Map<String, Object> loginOut(Map<String, String> reqBody) {
        String userId = reqBody.get("custNo");// 行员工号
        //删除Pad登录信息
        RedisUtils.deleteByKey(StringConstantUtil.TOKEN + userId);
        RedisUtils.deleteByKey(StringConstantUtil.USER + userId);
        //删除后管登录信息
        RedisUtils.deleteByKey(StringConstantUtil.MANAGE_TOKEN_ASSIST + userId);
        RedisUtils.deleteByKey(StringConstantUtil.MANAGE_USER_ASSIST + userId);
        return ResultUtil.resultMap("登出成功", ResultCode.RESULT_CODE_SUCCESS.code(), null);
    }
}
