package com.whjz.common.service;

import java.util.Map;

public interface IbsaccessLogService {

	/**
	 * 添加登陆日志
	 * @param reqBody
	 * @return
	 */
	public boolean insertIbsaccessLog(Map<String, String> reqBody);


}
