package com.whjz.manage.utils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.DocumentHelper;
import org.dom4j.Element;
import org.dom4j.Attribute;


import lombok.extern.slf4j.Slf4j;
@Slf4j
public class XmlUtil {

	
	public static Map<String, Object> getDateOfXML(String xmlFilePath) throws DocumentException {
		FileInputStream fis = null;
		Map<String, Object> map =null;
		try{
			fis = new FileInputStream(xmlFilePath);
			//将文件转换为字符串
			byte[] xmlByte = new byte[fis.available()];
			fis.read(xmlByte);
			String xmlStr = new String(xmlByte);
			//调用xml工具类，进行转换，转换成map
			log.info(">>>>>>>>>>>>>>>>>>>>>>>>> 文件解析开始 >>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			 map = XmlUtil.xml2mapWithAttr(xmlStr, false);
			log.info(">>>>>>>>>>>>>>>>>>>>>>>>> 文件解析结束 >>>>>>>>>>>>>>>>>>>>>>>>>>>>");
		}catch(IOException e){
			log.info(">>>>>>>>>>>>>>>>>>>>>>>>> 文件解析异常 >>>>>>>>>>>>>>>>>>>>>>>>>>>>"+xmlFilePath);
		}finally {
			try {
				if(fis!=null){
					fis.close();
				}
			}catch (Exception e){

			}
		}
		return map;
	}

	
	
	public static void main(String[] args) throws DocumentException {
		Map<String, Object> map=getDateOfXML("C:\\Users\\whjz\\Desktop\\20171221_6产品文件.xml");
		System.out.println(map.toString());
	}
	
	
	/** 
	 * xml转map 带属性 
	 * @param xmlStr 
	 * @param needRootKey 是否需要在返回的map里加根节点键 
	 * @return 
	 * @throws DocumentException 
	 */  
	public static Map xml2mapWithAttr(String xmlStr, boolean needRootKey) throws DocumentException {  
		Document doc = DocumentHelper.parseText(xmlStr);  
		Element root = doc.getRootElement();  
		Map<String, Object> map = (Map<String, Object>) xml2mapWithAttr(root);  
		if(root.elements().size()==0 && root.attributes().size()==0){  
			return map; //根节点只有一个文本内容  
		}  
		if(needRootKey){  
			//在返回的map里加根节点键（如果需要）  
			Map<String, Object> rootMap = new HashMap<String, Object>();  
			rootMap.put(root.getName(), map);  
			return rootMap;  
		}  
		return map;  
	}  
	/** 
	 * xml转map 带属性 
	 * @return
	 */  
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private static Map xml2mapWithAttr(Element element) {  
		Map<String, Object> map = new LinkedHashMap<String, Object>();  
		List<Element> list = element.elements();  
		List<Attribute> listAttr0 = element.attributes(); // 当前节点的所有属性的list  
		for (Attribute attr : listAttr0) {  
			map.put("@" + attr.getName(), attr.getValue());  
		}  
		if (list.size() > 0) {  
			for (int i = 0; i < list.size(); i++) {  
				Element iter = list.get(i);  
				List mapList = new ArrayList();  
				if (iter.elements().size() > 0) {  
					Map m = xml2mapWithAttr(iter);  
					if (map.get(iter.getName()) != null) {  
						Object obj = map.get(iter.getName());  
						if (!(obj instanceof List)) {  
							mapList = new ArrayList();  
							mapList.add(obj);  
							mapList.add(m);  
						}  
						if (obj instanceof List) {  
							mapList = (List) obj;  
							mapList.add(m);  
						}  
						map.put(iter.getName(), mapList);  
					} else  
						map.put(iter.getName(), m);  
				} else {  
					List<Attribute> listAttr = iter.attributes(); // 当前节点的所有属性的list  
					Map<String, Object> attrMap = null;  
					boolean hasAttributes = false;  
					if (listAttr.size() > 0) {  
						hasAttributes = true;  
						attrMap = new LinkedHashMap<String, Object>();  
						for (Attribute attr : listAttr) {  
							attrMap.put("@" + attr.getName(), attr.getValue());  
						}  
					}  
					if (map.get(iter.getName()) != null) {  
						Object obj = map.get(iter.getName());  
						if (!(obj instanceof List)) {  
							mapList = new ArrayList();  
							mapList.add(obj);  
							// mapList.add(iter.getText());  
							if (hasAttributes) {  
								attrMap.put("#text", iter.getText());  
								mapList.add(attrMap);  
							} else {  
								mapList.add(iter.getText());  
							}  
						}  
						if (obj instanceof List) {  
							mapList = (List) obj;  
							// mapList.add(iter.getText());  
							if (hasAttributes) {  
								attrMap.put("#text", iter.getText());  
								mapList.add(attrMap);  
							} else {  
								mapList.add(iter.getText());  
							}  
						}  
						map.put(iter.getName(), mapList);  
					} else {  
						// map.put(iter.getName(), iter.getText());  
						if (hasAttributes) {  
							attrMap.put("#text", iter.getText());  
							map.put(iter.getName(), attrMap);  
						} else {  
							map.put(iter.getName(), iter.getText());  
						}  
					}  
				}  
			}  
		} else {  
			// 根节点的  
			if (listAttr0.size() > 0) {  
				map.put("#text", element.getText());  
			} else {  
			 	map.put(element.getName(), element.getText());  
			}  
		}  
		return map;  
	}  

	
}
