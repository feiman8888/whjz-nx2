package com.whjz.manage.utils;

public enum LoginTypeEnum {

	TRADITION_LOGIN("1", "传统登录"), FINGERPRINT_LOGIN("2", "指纹登录"),  FACE_LOGIN("3", "人脸登录");

	private String code;

	private String desc;

	private LoginTypeEnum(String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

}
