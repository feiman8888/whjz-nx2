package com.whjz.manage.utils;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.MDC;

/**
 * @version 1.0
 * @Description todo
 * @Author lcc
 * @Date 2019/4/28 17:21
 **/
@Slf4j
public class LogUtils {

    public static void info(String message){
        MDC.put("userId","0500");
        log.info(message);
    }
}
