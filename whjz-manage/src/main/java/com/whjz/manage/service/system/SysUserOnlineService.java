package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-12
 * remarks：机构类别相关接口
 */

public interface SysUserOnlineService {

    Map<String, Object> queryUserOnlineList(Map<String, String> paramMap);

    Map<String, Object> insertUserOnline(Map<String, String> paramMap);

    Map<String, Object> updateUserOnline(Map<String, String> paramMap);

    Map<String, Object> deleteUserOnline(Map<String, String> paramMap);
}
