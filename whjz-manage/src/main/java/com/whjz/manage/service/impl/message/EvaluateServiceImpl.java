package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.Evaluate;
import com.whjz.manage.dao.message.EvaluateMapper;
import com.whjz.manage.service.message.EvaluateService;
import com.whjz.manage.service.message.EvaluateService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：图片相关接口
 */
@Slf4j
@Service(value = "EvaluateService")
@SuppressWarnings("all")
public class EvaluateServiceImpl implements EvaluateService {

    @Autowired
    private EvaluateMapper evaluateMapper;

    /**
     * @param paramMap 查询用户条件
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryEvaluateList(Map<String, String> paramMap) {
        try {
            List<Evaluate> evaluateList = evaluateMapper.queryEvaluateList(paramMap);
            int evaluateCount = evaluateMapper.queryEvaluateCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, evaluateList, evaluateCount));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 用户相关信息
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertEvaluate(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            boolean flag = evaluateMapper.insertEvaluate(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 用户相关信息
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateEvaluate(Map<String, String> paramMap) {
        try {
            boolean flag = evaluateMapper.modifyEvaluate(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteEvaluate(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    evaluateMapper.deleteEvaluate(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
