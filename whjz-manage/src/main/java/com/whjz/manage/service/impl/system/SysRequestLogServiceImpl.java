package com.whjz.manage.service.impl.system;

import com.whjz.manage.dao.system.SysRequestLogMapper;
import com.whjz.entity.system.SysRequestLog;
import com.whjz.manage.service.system.SysRequestLogService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-22
 * remarks：机构相关接口
 */
@Slf4j
@Service(value = "sysRequestLogService")
@SuppressWarnings("all")
public class SysRequestLogServiceImpl implements SysRequestLogService {

    @Autowired
    private SysRequestLogMapper sysRequestLogMapper;

    /**
     *
     * @param paramMap  查询机构条件
     * @return  查询成功或失败
     */
    @Override
    public Map<String, Object> queryRequestLogList(Map<String, String> paramMap) {
        try {
            List<SysRequestLog> sysUserList = sysRequestLogMapper.queryRequestLogList(paramMap);
            int count = sysRequestLogMapper.queryRequestLogCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, sysUserList, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  机构相关信息
     * @return  添加成功或失败
     */
    @Override
    public Map<String, Object> insertRequestLog(Map<String, String> paramMap) {
        try {
//            paramMap.put("userId", "sysuser" + System.currentTimeMillis() + (new Random().nextInt(99999 - 10000) + 10000 + 1));
            String date  = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag = sysRequestLogMapper.insertRequestLog(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  机构相关信息
     * @return  修改成功或失败
     */
    @Override
    public Map<String, Object> updateRequestLog(Map<String, String> paramMap) {
        try {
            String date  = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);
            boolean flag = sysRequestLogMapper.modifyRequestLog(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param 字符串类型的ID
     * @return
     */
    @Override
    public Map<String, Object> deleteRequestLog(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    sysRequestLogMapper.deleteRequestLog(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
