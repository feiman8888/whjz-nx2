package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.Order;
import com.whjz.manage.dao.message.OrderMapper;
import com.whjz.manage.service.message.OrderService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：订单相关接口
 */
@Slf4j
@Service(value = "orderService")
@SuppressWarnings("all")
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderMapper orderMapper;

    /**
     * @param paramMap 查询订单条件
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryOrderList(Map<String, String> paramMap) {
        try {
            List<Order> orderList = orderMapper.queryOrderList(paramMap);
            int orderCount = orderMapper.queryOrderCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, orderList, orderCount));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 订单相关信息
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertOrder(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            boolean flag = orderMapper.insertOrder(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 订单相关信息
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateOrder(Map<String, String> paramMap) {
        try {
            boolean flag = orderMapper.modifyOrder(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteOrder(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    orderMapper.deleteOrder(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
