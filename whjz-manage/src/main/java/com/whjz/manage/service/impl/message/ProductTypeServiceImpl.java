package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.ProductType;
import com.whjz.manage.dao.message.ProductTypeMapper;
import com.whjz.manage.service.message.ProductTypeService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-10-11
 * remarks：产品分类相关接口
 */
@Slf4j
@Service(value = "productTypeService")
@SuppressWarnings("all")
public class ProductTypeServiceImpl implements ProductTypeService {

    @Autowired
    private ProductTypeMapper productTypeMapper;

    /**
     * @param paramMap 查询产品分类条件
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryProductTypeList(Map<String, String> paramMap) {
        try {
            List<ProductType> productTypeList = productTypeMapper.queryProductTypeList(paramMap);
            int productTypeCount = productTypeMapper.queryProductTypeCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, productTypeList, productTypeCount));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 产品分类相关信息
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertProductType(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            boolean flag = productTypeMapper.insertProductType(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 产品分类相关信息
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateProductType(Map<String, String> paramMap) {
        try {
            boolean flag = productTypeMapper.modifyProductType(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteProductType(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    productTypeMapper.deleteProductType(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
