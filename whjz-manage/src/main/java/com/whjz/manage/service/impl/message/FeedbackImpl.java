package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.Feedback;
import com.whjz.manage.dao.message.FeedbackMapper;
import com.whjz.manage.service.message.FeedbackService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author:hj
 * date:2020-10-11
 * remarks:常见问题类
 */
@Slf4j
@Service(value ="feedback" )
@SuppressWarnings("all")
public class FeedbackImpl implements FeedbackService {

    @Autowired
    private FeedbackMapper feedbackMapper;

    /**
     * @param paramMap 添加意见反馈
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertFeedback(Map<String, String> paramMap) {
        try {
            String date  = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag =feedbackMapper.insertFeedback(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }


    /**
     * @param paramMap 修改意见反馈
     * @return 添加成功或失败
     */
      @Override
    public Map<String, Object> updateFeedback(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);

            boolean flag = feedbackMapper.updateFeedback (paramMap);

            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }


    /**
     * @param paramMap 删除意见反馈
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> deleteFeedback(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoId") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoId")))) {
                String autoIds = paramMap.get("autoId");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {

                    feedbackMapper.deleteFeedback(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }


    /**
     * @param paramMap 查询意见反馈
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> queryFeedbackList(Map<String, String> paramMap) {
        try {
            List<Feedback> queryFeedback =feedbackMapper.queryFeedbackList(paramMap);
            int count = feedbackMapper.queryFeedbackCount(paramMap);

            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap,queryFeedback,count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
