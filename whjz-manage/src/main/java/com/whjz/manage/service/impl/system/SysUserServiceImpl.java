package com.whjz.manage.service.impl.system;

import com.alibaba.fastjson.JSON;
import com.whjz.manage.dao.system.SysUserMapper;
import com.whjz.entity.system.SysUser;
import com.whjz.manage.logic.UserLogic;
import com.whjz.manage.service.system.SysUserService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：用户相关接口
 */
@Slf4j
@Service(value = "sysUserService")
@SuppressWarnings("all")
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    private SysUserMapper sysUserMapper;

    /**
     * @param paramMap 查询用户条件
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryUserList(Map<String, String> paramMap) {
        try {
            List<SysUser> sysUserList = sysUserMapper.queryUserList(paramMap);
            int userCount = sysUserMapper.queryUserCount(paramMap);
//            return ResultUtil.resultMap(CodeUtil.CODE_200.getCode(), CodeUtil.CODE_200.getName(), sysUserList);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, sysUserList, userCount));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 用户相关信息
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertUser(Map<String, String> paramMap) {
        try {
//            paramMap.put("userId", "sysuser" + System.currentTimeMillis() + (new Random().nextInt(99999 - 10000) + 10000 + 1));
            if (StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("userNetName")))) {
                paramMap.put("userNetName", StringUtils.getString(paramMap.get("phone")));
            }
            //对用户密码进行二次转加密
            String newPassword = UserLogic.MD5(StringUtils.getString(paramMap.get("userPasswd")));
            String date = DateUtils.getCurDateTime();
            paramMap.put("userPasswd", newPassword);
            paramMap.put("userPasswdUpdate", DateUtils.getCurDate().replaceAll("-", ""));
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            paramMap.put("ext1", StringConstantUtil.RESETPASSWORD);
            boolean flag = sysUserMapper.insertUser(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap 用户相关信息
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateUser(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("userPasswdUpdate", DateUtils.getCurDate().replaceAll("-", ""));
            paramMap.put("updateDate", date);
            boolean flag = sysUserMapper.modifyUser(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteUser(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    SysUser sysUser = sysUserMapper.queryUserById(autoId);
                    sysUser.setUserState(StringConstantUtil.USERSTATESDELETE);
                    Map<String, String> userParamMap = JSON.parseObject(JSON.toJSONString(sysUser), Map.class);
                    sysUserMapper.modifyUser(userParamMap);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap 自增ID
     * @return
     */
    @Override
    public Map<String, Object> frozenUser(Map<String, String> paramMap) {
        try {
            String autoId = StringUtils.getString(paramMap);
            SysUser sysUser = sysUserMapper.queryUserById(autoId);
            if (sysUser != null) {
                sysUser.setUserState(StringConstantUtil.USERSTATESFROZEN);
                Map<String, String> userParamMap = JSON.parseObject(JSON.toJSONString(sysUser), Map.class);
                boolean flag = sysUserMapper.modifyUser(userParamMap);
                if (flag) {
                    return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap 自增ID
     * @return
     */
    @Override
    public Map<String, Object> resetUserPwd(Map<String, String> paramMap) {
        try {
            String autoId = StringUtils.getString(paramMap);
            SysUser sysUser = sysUserMapper.queryUserById(autoId);
            if (sysUser != null) {
                //第一次加密
                String password  =  UserLogic.MD5(sysUser.getPhone().substring(3, sysUser.getPhone().length()));
                //第二次加密
                password = UserLogic.MD5(password);
                sysUser.setUserPasswd(password);
                Map<String, String> userParamMap = JSON.parseObject(JSON.toJSONString(sysUser), Map.class);
                boolean flag = sysUserMapper.modifyUser(userParamMap);
                if (flag) {
                    return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
                }
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
