package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-12
 * remarks：机构类别相关接口
 */

public interface SysDeptService {

    Map<String, Object> queryDeptList(Map<String, String> paramMap);

    Map<String, Object> insertDept(Map<String, String> paramMap);

    Map<String, Object> updateDept(Map<String, String> paramMap);

    Map<String, Object> deleteDept(Map<String, String> paramMap);
}
