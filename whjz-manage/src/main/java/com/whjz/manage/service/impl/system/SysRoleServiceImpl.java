package com.whjz.manage.service.impl.system;

import com.whjz.manage.dao.system.SysRoleMapper;
import com.whjz.entity.system.SysRole;
import com.whjz.manage.service.system.SysRoleService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：角色相关接口
 */
@Slf4j
@Service(value = "sysRoleService")
@SuppressWarnings("all")
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    private SysRoleMapper sysRoleMapper;

    /**
     *
     * @param paramMap  查询角色条件
     * @return  查询成功或失败
     */
    @Override
    public Map<String, Object> queryRoleList(Map<String, String> paramMap) {
        try {
            List<SysRole> sysUserList = sysRoleMapper.queryRoleList(paramMap);
            int count = sysRoleMapper.queryRoleCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, sysUserList, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  角色相关信息
     * @return  添加成功或失败
     */
    @Override
    public Map<String, Object> insertRole(Map<String, String> paramMap) {
        try {
//            paramMap.put("userId", "sysuser" + System.currentTimeMillis() + (new Random().nextInt(99999 - 10000) + 10000 + 1));
            String date  = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            paramMap.put("roleState", StringConstantUtil.ROLESTATESNORMAL);
            boolean flag = sysRoleMapper.insertRole(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  角色相关信息
     * @return  修改成功或失败
     */
    @Override
    public Map<String, Object> updateRole(Map<String, String> paramMap) {
        try {
            String date  = DateUtils.getCurDateTime();
            paramMap.put("userPasswdUpdate", DateUtils.getCurDate().replaceAll("-", ""));
            paramMap.put("updateDate", date);
            boolean flag = sysRoleMapper.modifyRole(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteRole(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    sysRoleMapper.deleteRole(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
