package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-12
 * remarks:银行卡管理接口
 */
public interface BankCardService  {

    Map<String, Object> queryBankCardList(Map<String, String> paramMap);

    Map<String, Object> insertBankCard(Map<String, String> paramMap);

    Map<String, Object> updateBankCard(Map<String, String> paramMap);

    Map<String, Object> deleteBankCard(Map<String, String> paramMap);
}
