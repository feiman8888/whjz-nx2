package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-10
 * remarks：兼职相关接口
 */

public interface ContractService {

    Map<String, Object> queryContractList(Map<String, String> paramMap);

    Map<String, Object> insertContract(Map<String, String> paramMap);

    Map<String, Object> deleteContract(Map<String, String> paramMap);

    Map<String, Object> updateContract(Map<String, String> paramMap);
}
