package com.whjz.manage.service.system;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-25
 * remarks：登录日志相关接口
 */

public interface SysIbsaccessLogForLoginService {

    Map<String, Object> queryIbsaccessLogForLoginList(Map<String, String> paramMap);

    Map<String, Object> insertIbsaccessLogForLogin(Map<String, String> paramMap);

    Map<String, Object> updateIbsaccessLogForLogin(Map<String, String> paramMap);

    Map<String, Object> deleteIbsaccessLogForLogin(Map<String, String> paramMap);
}
