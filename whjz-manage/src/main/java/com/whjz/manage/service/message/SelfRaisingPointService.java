package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-12
 * remarks：兼职相关接口
 */

public interface SelfRaisingPointService {

    Map<String, Object> querySelfRaisingPointList(Map<String, String> paramMap);

    Map<String, Object> insertSelfRaisingPoint(Map<String, String> paramMap);

    Map<String, Object> deleteSelfRaisingPoint(Map<String, String> paramMap);

    Map<String, Object> updateSelfRaisingPoint(Map<String, String> paramMap);
}
