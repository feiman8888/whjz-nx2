package com.whjz.manage.service.impl.system;

import com.whjz.manage.dao.system.SysRoleMenuMapper;
import com.whjz.entity.system.SysRoleMenu;
import com.whjz.manage.service.system.SysRoleMenuService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-22
 * remarks：角色菜单相关接口
 */
@Slf4j
@Service(value = "sysRoleMenuService")
@SuppressWarnings("all")
public class SysRoleMenuServiceImpl implements SysRoleMenuService {

    @Autowired
    private SysRoleMenuMapper sysRoleMenuMapper;

    /**
     *
     * @param paramMap  查询角色菜单条件
     * @return  查询成功或失败
     */
    @Override
    public Map<String, Object> queryRoleMenuList(Map<String, String> paramMap) {
        try {
            List<SysRoleMenu> sysUserList = sysRoleMenuMapper.queryRoleMenuList(paramMap);
            int count = sysRoleMenuMapper.queryRoleMenuCount(paramMap);
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, sysUserList, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  角色菜单相关信息
     * @return  添加成功或失败
     */
    @Override
    public Map<String, Object> insertRoleMenu(Map<String, String> paramMap) {
        try {
//            paramMap.put("userId", "sysuser" + System.currentTimeMillis() + (new Random().nextInt(99999 - 10000) + 10000 + 1));
            String date  = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            paramMap.put("state", StringConstantUtil.ROLEMENUSTATESNORMAL);
            boolean flag = sysRoleMenuMapper.insertRoleMenu(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap  角色菜单相关信息
     * @return  修改成功或失败
     */
    @Override
    public Map<String, Object> updateRoleMenu(Map<String, String> paramMap) {
        try {
            String date  = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);
            boolean flag = sysRoleMenuMapper.modifyRoleMenu(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(),e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     *
     * @param paramMap
     * @return
     */
    @Override
    public Map<String, Object> deleteRoleMenu(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoIds") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoIds")))) {
                String autoIds = paramMap.get("autoIds");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {
                    sysRoleMenuMapper.deleteRoleMenu(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }
}
