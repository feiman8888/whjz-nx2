package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.Supplier;
import com.whjz.manage.dao.message.SupplierMapper;
import com.whjz.manage.service.message.SupplierService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;
/**
 * author:hj
 * date:2020-10-10
 * remarks:供应商类
 */
@Slf4j
@Service(value ="supplier" )
@SuppressWarnings("all")
public class SupplierImpl implements SupplierService {

    @Autowired
    private SupplierMapper supplierMapper;


    /**
     * @param paramMap 供应商
     * @return 查询成功或失败
     */  @Override
    public Map<String, Object> querySupplierList(Map<String, String> paramMap) {
        try {
            List<Supplier> querySupplierList = supplierMapper.querySupplierList(paramMap);
            int count = supplierMapper.querySupplierCount(paramMap);

            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap, querySupplierList, count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**
     * s
     *
     * @param paramMap 添加供应商
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertSupplier(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag = supplierMapper.insertSupplier(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }


    /**
     * @param paramMap 供应商
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateSupplier(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);

            boolean flag = supplierMapper.updateSupplier(paramMap);

            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }


    /**
     * @param paramMap 供应商
     * @return 删除成功或失败
     */ @Override
    public Map<String, Object> deleteSupplier(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoId") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoId")))) {
                String autoIds = paramMap.get("autoId");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {

                    supplierMapper.deleteSupplier(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);

    }

}