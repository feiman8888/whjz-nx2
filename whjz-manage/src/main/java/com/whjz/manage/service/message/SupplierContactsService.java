package com.whjz.manage.service.message;

import java.util.Map;

/**
 * author:hj
 * data:2020-10-6
 * remarks:供应商联系人管理接口
 */

public interface SupplierContactsService {



    Map<String, Object> querySupplierContactsList(Map<String, String> paramMap);

    Map<String, Object> insertSupplierContacts(Map<String, String> paramMap);

    Map<String, Object> updateSupplierContacts(Map<String, String> paramMap);

    Map<String, Object> deleteSupplierContacts(Map<String, String> paramMap);

}
