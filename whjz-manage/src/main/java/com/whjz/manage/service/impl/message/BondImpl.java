package com.whjz.manage.service.impl.message;

import com.whjz.entity.message.Bond;
import com.whjz.manage.dao.message.BondMapper;
import com.whjz.manage.service.message.BondService;
import com.whjz.utils.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

/**
 * author:hj
 * date:2020-10-9
 * remarks:供应商保证金类
 */
@Slf4j
@Service(value ="Bond" )
@SuppressWarnings("all")

public class BondImpl implements BondService{

    @Autowired
    private BondMapper bondMapper;
    /**
     * @param paramMap 查询供应商保证金
     * @return 查询成功或失败
     */
    @Override
    public Map<String, Object> queryBondList(Map<String, String> paramMap) {
        try {
            List<Bond> queryBond =bondMapper.queryBondList(paramMap);
            int count = bondMapper.queryBondCount(paramMap);

            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), PageUtil.getPageMap(paramMap,queryBond,count));
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }

    /**s
     * @param paramMap 添加供应商保证金
     * @return 添加成功或失败
     */
    @Override
    public Map<String, Object> insertBond(Map<String, String> paramMap) {
        try {
            String date  = DateUtils.getCurDateTime();
            paramMap.put("createDate", date);
            paramMap.put("updateDate", date);
            boolean flag =bondMapper.insertBond(paramMap);
            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }




    /**
     * @param paramMap 修改供应商保证金
     * @return 修改成功或失败
     */
    @Override
    public Map<String, Object> updateBond(Map<String, String> paramMap) {
        try {
            String date = DateUtils.getCurDateTime();
            paramMap.put("updateDate", date);

            boolean flag = bondMapper.updateBond (paramMap);

            if (flag) {
                return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);
    }



    /**
     *
     * @param paramMap 删除供应商保证金
     * @return 删除成功或失败
     */
    @Override
    public Map<String, Object> deleteBond(Map<String, String> paramMap) {
        try {
            if (paramMap.containsKey("autoId") && StringUtils.isNotNullOrEmtory(StringUtils.getString(paramMap.get("autoId")))) {
                String autoIds = paramMap.get("autoId");
                String[] autoIdArray = autoIds.split(",");
                for (String autoId : autoIdArray) {

                    bondMapper.deleteBond(autoId);
                }
            }
            return ResultUtil.resultMap(CodeUtil.CODE_200.getName(), CodeUtil.CODE_200.getCode(), null);
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_500.getName(), CodeUtil.CODE_500.getCode(), null);

    }
}
