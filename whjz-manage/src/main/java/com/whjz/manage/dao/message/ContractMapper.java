package com.whjz.manage.dao.message;

import com.whjz.entity.message.Contract;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：合同相关接口
 */

public interface ContractMapper {
    //查询合同
    List<Contract> queryContractList(Map<String, String> paramMap);

    //添加合同
    boolean insertContract(Map<String, String> paramMap);

    //根据ID查询合同
    Contract queryContractById(@Param("autoId") String autoId);

    //修改合同
    boolean modifyContract(Map<String, String> paramMap);

    //根据查询条件查询总记录数
    int queryContractCount(Map<String, String> paramMap);

    //删除合同管理
    boolean deleteContract(@Param("autoId") String autoId);
}
