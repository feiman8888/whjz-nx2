package com.whjz.manage.dao.message;

import com.whjz.entity.message.Picture;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：合同相关接口
 */

public interface PictureMapper {
    //查询合同
    List<Picture> queryPictureList(Map<String, String> paramMap);

    //添加合同
    boolean insertPicture(Map<String, String> paramMap);

    //根据ID查询合同
    Picture queryPictureById(@Param("autoId") String autoId);

    //修改合同
    boolean modifyPicture(Map<String, String> paramMap);

    //根据查询条件查询总记录数
    int queryPictureCount(Map<String, String> paramMap);

    //删除合同管理
    boolean deletePicture(@Param("autoId") String autoId);
}
