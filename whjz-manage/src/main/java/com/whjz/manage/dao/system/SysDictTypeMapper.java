package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysDictType;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：字典类别相关接口
 */

public interface SysDictTypeMapper {
    //查询字典类别
    List<SysDictType> queryDictTypeList(Map<String, String> paramMap);

    //添加字典类别
    boolean insertDictType(Map<String, String> paramMap);

    //修改字典类别
    boolean modifyDictType(Map<String, String> paramMap);

    //删除字典类别
    boolean deleteDictType(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryDictTypeCount(Map<String, String> paramMap);

    //根据ID查询字典类别
    SysDictType queryDictTypeById(@Param("autoId") String autoId);
}
