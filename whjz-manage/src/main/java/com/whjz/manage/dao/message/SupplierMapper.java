package com.whjz.manage.dao.message;

import com.whjz.entity.message.Supplier;
import feign.Param;

import java.util.List;
import java.util.Map;
/**
 * author:hj
 * date:2020-10-10
 * remarks:供应商接口
 */

public interface SupplierMapper {

    //查询供应商
    List<Supplier> querySupplierList(Map<String, String> paramMap);
    //增加供应商
    boolean insertSupplier(Map<String, String> paraMap);
    //修改供应商
    boolean updateSupplier(Map<String, String> parMap);
    //删除供应商
    boolean deleteSupplier(@Param("autoId") String autoId);
    //根据条件查询总记录数
    int querySupplierCount(Map<String, String> paramMap);
}
