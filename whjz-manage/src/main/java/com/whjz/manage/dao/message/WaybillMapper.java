package com.whjz.manage.dao.message;

import com.whjz.entity.message.Waybill;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author:hj
 * date:2020-10-13
 * remarks:运单信息
 */

public interface WaybillMapper {
    //查询运单信息
    List<Waybill> queryWaybillList(Map<String, String> paramMap);
    //增加运单信息
    boolean insertWaybill(Map<String,String >paraMap);
    //修改运单信息
    boolean updateWaybill(Map<String, String> parMap);
    //删除运单信息
    boolean deleteWaybill(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryWaybillCount(Map<String,String> paramMap);
}

