package com.whjz.manage.dao.message;

import com.whjz.entity.message.Bank;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author:hj
 * date:2020-10-12
 * remarks:银行管理接口
 */
public interface BankMapper {
    //查询银行管理
    List<Bank> queryBankList(Map<String, String> paramMap);
    //增加银行管理
    boolean insertBank(Map<String,String >paraMap);
    //修改银行管理
    boolean updateBank(Map<String, String> parMap);
    //删除银行管理
    boolean deleteBank(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryBankCount(Map<String,String> paramMap);
}
