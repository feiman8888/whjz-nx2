package com.whjz.manage.dao.message;

import com.whjz.entity.message.Feedback;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 *  author: hj
 *  date: 2020-10-12
 *  remarks：意见反馈接口
 */
public interface FeedbackMapper {
    //查询意见反馈
    List<Feedback> queryFeedbackList(Map<String, String> paramMap);
    //增加意见反馈
    boolean insertFeedback(Map<String,String >paraMap);
    //修改意见反馈
    boolean updateFeedback(Map<String, String> parMap);
    //删除意见反馈
    boolean deleteFeedback(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryFeedbackCount(Map<String,String> paramMap);
}
