package com.whjz.manage.dao.message;

import com.whjz.entity.message.ShoppingCart;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-10-12
 * remarks：购物车相关接口
 */

public interface ShoppingCartMapper {
    //查询合同
    List<ShoppingCart> queryShoppingCartList(Map<String, String> paramMap);

    //添加合同
    boolean insertShoppingCart(Map<String, String> paramMap);

    //根据ID查询合同
    ShoppingCart queryShoppingCartById(@Param("autoId") String autoId);

    //修改合同
    boolean modifyShoppingCart(Map<String, String> paramMap);

    //根据查询条件查询总记录数
    int queryShoppingCartCount(Map<String, String> paramMap);

    //删除合同管理
    boolean deleteShoppingCart(@Param("autoId") String autoId);
}
