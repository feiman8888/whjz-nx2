package com.whjz.manage.dao.system;

import com.whjz.entity.system.SysRole;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：角色相关接口
 */

public interface SysRoleMapper {
    //查询角色
    List<SysRole> queryRoleList(Map<String, String> paramMap);

    //添加角色
    boolean insertRole(Map<String, String> paramMap);

    //修改角色
    boolean modifyRole(Map<String, String> paramMap);

    //删除角色
    boolean deleteRole(@Param("autoId") String autoId);

    //根据条件查询总记录数
    int queryRoleCount(Map<String, String> paramMap);
}
