package com.whjz.manage.dao.message;

import com.whjz.entity.message.SelfRaisingPoint;
import feign.Param;

import java.util.List;
import java.util.Map;

/**
 * author: fln
 * date: 2020-10-12
 * remarks：自提点相关接口
 */

public interface SelfRaisingPointMapper {
    //查询自提点
    List<SelfRaisingPoint> querySelfRaisingPointList(Map<String, String> paramMap);

    //添加自提点
    boolean insertSelfRaisingPoint(Map<String, String> paramMap);

    //根据ID查询自提点
    SelfRaisingPoint querySelfRaisingPointById(@Param("autoId") String autoId);

    //修改自提点
    boolean modifySelfRaisingPoint(Map<String, String> paramMap);

    //根据查询条件查询总记录数
    int querySelfRaisingPointCount(Map<String, String> paramMap);

    //删除自提点管理
    boolean deleteSelfRaisingPoint(@Param("autoId") String autoId);
}
