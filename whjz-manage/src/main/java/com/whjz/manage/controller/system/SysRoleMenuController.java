package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysRoleMenuService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：角色菜单表相关接口
 */

@Api(tags = {"角色菜单表管理"})
@RestController
@Slf4j
@RequestMapping("sysRoleMenu")
public class SysRoleMenuController extends AbstractControllerImpl {

    @Autowired
    private SysRoleMenuService sysRoleMenuService;

    @PostMapping("/queryRoleMenuList")
    @ApiOperation(value = "查询角色菜单表列表", notes = "根据查询条件返回角色菜单表列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryRoleMenuList(@RequestBody Map<String,String> paramMap) {
        return sysRoleMenuService.queryRoleMenuList(paramMap);
    }

    @PostMapping("/insertRoleMenu")
    @ApiOperation(value = "添加角色菜单表", notes = "添加角色菜单表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "roleCode",value = "角色编码",paramType = "query", required = true),
            @ApiImplicitParam(name = "menuCode",value = "菜单编码",paramType = "query", required = true),
            @ApiImplicitParam(name = "operator",value = "操作人",paramType = "query", required = true),
            @ApiImplicitParam(name = "state",value = "状态标识",paramType = "query"),
            @ApiImplicitParam(name = "createDate",value = "创建时间",paramType = "query"),
            @ApiImplicitParam(name = "updateDate",value = "更新时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> insertRoleMenu(@RequestBody Map<String,String> paramMap) {
        return sysRoleMenuService.insertRoleMenu(paramMap);
    }

    @PostMapping("/updateRoleMenu")
    @ApiOperation(value = "修改角色菜单表", notes = "修改角色菜单表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "自增Id",paramType = "query", required = true),
            @ApiImplicitParam(name = "roleCode",value = "角色编码",paramType = "query", required = true),
            @ApiImplicitParam(name = "menuCode",value = "菜单编码",paramType = "query", required = true),
            @ApiImplicitParam(name = "operator",value = "操作人",paramType = "query", required = true),
            @ApiImplicitParam(name = "state",value = "状态标识",paramType = "query"),
            @ApiImplicitParam(name = "createDate",value = "创建时间",paramType = "query"),
            @ApiImplicitParam(name = "updateDate",value = "更新时间",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
    })
    public Map<String,Object> updateRoleMenu(@RequestBody Map<String,String> paramMap) {
        return sysRoleMenuService.updateRoleMenu(paramMap);
    }

    @PostMapping("/deleteRoleMenu")
    @ApiOperation(value = "删除角色菜单表", notes = "删除角色菜单表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteRoleMenu(@RequestBody Map<String,String> paramMap) {
        return sysRoleMenuService.deleteRoleMenu(paramMap);
    }
}
