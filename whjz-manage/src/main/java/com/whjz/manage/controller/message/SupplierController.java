package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.SupplierService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-10
 * remarks:供应商类
 */
@Api(tags = "供应商")
@Slf4j
@RestController
@RequestMapping("supplier")
public class SupplierController {
    @Autowired
    @SuppressWarnings("all")
    private SupplierService supplierService;

    @PostMapping("/querySupplierList")

    @ApiOperation(value = "查询供应商列表", notes = "根据条件查询供应商返回供应商列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "enterpriseType", value = "企业类型", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "供应商名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentDate", value = "经营范围", paramType = "query", required = true),
            @ApiImplicitParam(name = "registerCapital", value = "注册资本", paramType = "query", required = true),
            @ApiImplicitParam(name = "registerDate", value = "注册日期", paramType = "query", required = true),
            @ApiImplicitParam(name = "businessTerm", value = "营业期限", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierGeneralization", value = "供应商概况", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierAddress", value = "供应商地址", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierPhone", value = "供应商电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLegalpersonName", value = "供应商法人姓名", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLegalpersonPhone", value = "供应商法人电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLoginNumber", value = "供应商登录账号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierFax", value = "供应商传真", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierEmail", value = "供应商邮箱", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierChargepersonName", value = "供应商负责人姓名", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierChargepersonPhone", value = "负责人电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplyCapacity", value = "供应能力", paramType = "query", required = true),
            @ApiImplicitParam(name = "developmentPotential", value = "发展潜力", paramType = "query", required = true),
            @ApiImplicitParam(name = "enterpriseScale", value = "企业规模", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierBank", value = "供应商开户银行", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierAccountNumber", value = "供应商开户账号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierTaxNumber", value = "供应商税号", paramType = "query", required = true),
            @ApiImplicitParam(name = "businessLicense", value = "供应商营业执照", paramType = "query", required = true),
            @ApiImplicitParam(name = "certificateExpirationDueDate", value = "营业执照到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "productionLicense", value = "生产许可证", paramType = "query", required = true),
            @ApiImplicitParam(name = "productionLicenseDueDate", value = "生产许可证到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "qualitySystemCertificate", value = "质量体系证书", paramType = "query", required = true),
            @ApiImplicitParam(name = "qualitySystemCertificateDueDate", value = "质量体系证书证件到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "productIdentification", value = "产品标识", paramType = "query", required = true),
            @ApiImplicitParam(name = "industryMonitoringReport", value = "行业检测报告", paramType = "query", required = true),
            @ApiImplicitParam(name = "productCode", value = "产品编码", paramType = "query", required = true),
            @ApiImplicitParam(name = "contractExpirationDate", value = "合约到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLevel", value = "供应商等级", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierAccountingPeriod", value = "供应商账期", paramType = "query", required = true),
            @ApiImplicitParam(name = "createSupplierRatingDate", value = "供应商评分", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierQualityEngineer", value = "供应商质量工程师", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext6", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext7", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext8", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext9", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> querySupplierList(@RequestBody Map<String, String> paramMap) {

        return supplierService.querySupplierList(paramMap);
    }
    @PostMapping("/insertSupplier")

    @ApiOperation(value = "添加供应商列表", notes = "添加供应商列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "enterpriseType", value = "企业类型", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "供应商名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentDate", value = "经营范围", paramType = "query", required = true),
            @ApiImplicitParam(name = "registerCapital", value = "注册资本", paramType = "query", required = true),
            @ApiImplicitParam(name = "registerDate", value = "注册日期", paramType = "query", required = true),
            @ApiImplicitParam(name = "businessTerm", value = "营业期限", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierGeneralization", value = "供应商概况", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierAddress", value = "供应商地址", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierPhone", value = "供应商电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLegalpersonName", value = "供应商法人姓名", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLegalpersonPhone", value = "供应商法人电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLoginNumber", value = "供应商登录账号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierFax", value = "供应商传真", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierEmail", value = "供应商邮箱", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierChargepersonName", value = "供应商负责人姓名", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierChargepersonPhone", value = "负责人电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplyCapacity", value = "供应能力", paramType = "query", required = true),
            @ApiImplicitParam(name = "developmentPotential", value = "发展潜力", paramType = "query", required = true),
            @ApiImplicitParam(name = "enterpriseScale", value = "企业规模", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierBank", value = "供应商开户银行", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierAccountNumber", value = "供应商开户账号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierTaxNumber", value = "供应商税号", paramType = "query", required = true),
            @ApiImplicitParam(name = "businessLicense", value = "供应商营业执照", paramType = "query", required = true),
            @ApiImplicitParam(name = "certificateExpirationDueDate", value = "营业执照到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "productionLicense", value = "生产许可证", paramType = "query", required = true),
            @ApiImplicitParam(name = "productionLicenseDueDate", value = "生产许可证到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "qualitySystemCertificate", value = "质量体系证书", paramType = "query", required = true),
            @ApiImplicitParam(name = "qualitySystemCertificateDueDate", value = "质量体系证书证件到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "productIdentification", value = "产品标识", paramType = "query", required = true),
            @ApiImplicitParam(name = "industryMonitoringReport", value = "行业检测报告", paramType = "query", required = true),
            @ApiImplicitParam(name = "productCode", value = "产品编码", paramType = "query", required = true),
            @ApiImplicitParam(name = "contractExpirationDate", value = "合约到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLevel", value = "供应商等级", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierAccountingPeriod", value = "供应商账期", paramType = "query", required = true),
            @ApiImplicitParam(name = "createSupplierRatingDate", value = "供应商评分", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierQualityEngineer", value = "供应商质量工程师", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext6", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext7", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext8", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext9", value = "扩展字段5", paramType = "query")
    })

    public Map<String, Object>insertSupplier(@RequestBody Map<String, String> paramMap) {

        return supplierService.insertSupplier(paramMap);
    }
    @PostMapping("/updateSupplier")

    @ApiOperation(value = "修改供应商列表", notes = "修改供应商列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "enterpriseType", value = "企业类型", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "供应商名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentDate", value = "经营范围", paramType = "query", required = true),
            @ApiImplicitParam(name = "registerCapital", value = "注册资本", paramType = "query", required = true),
            @ApiImplicitParam(name = "registerDate", value = "注册日期", paramType = "query", required = true),
            @ApiImplicitParam(name = "businessTerm", value = "营业期限", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierGeneralization", value = "供应商概况", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierAddress", value = "供应商地址", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierPhone", value = "供应商电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLegalpersonName", value = "供应商法人姓名", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLegalpersonPhone", value = "供应商法人电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLoginNumber", value = "供应商登录账号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierFax", value = "供应商传真", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierEmail", value = "供应商邮箱", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierChargepersonName", value = "供应商负责人姓名", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierChargepersonPhone", value = "负责人电话", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplyCapacity", value = "供应能力", paramType = "query", required = true),
            @ApiImplicitParam(name = "developmentPotential", value = "发展潜力", paramType = "query", required = true),
            @ApiImplicitParam(name = "enterpriseScale", value = "企业规模", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierBank", value = "供应商开户银行", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierAccountNumber", value = "供应商开户账号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierTaxNumber", value = "供应商税号", paramType = "query", required = true),
            @ApiImplicitParam(name = "businessLicense", value = "供应商营业执照", paramType = "query", required = true),
            @ApiImplicitParam(name = "certificateExpirationDueDate", value = "营业执照到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "productionLicense", value = "生产许可证", paramType = "query", required = true),
            @ApiImplicitParam(name = "productionLicenseDueDate", value = "生产许可证到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "qualitySystemCertificate", value = "质量体系证书", paramType = "query", required = true),
            @ApiImplicitParam(name = "qualitySystemCertificateDueDate", value = "质量体系证书证件到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "productIdentification", value = "产品标识", paramType = "query", required = true),
            @ApiImplicitParam(name = "industryMonitoringReport", value = "行业检测报告", paramType = "query", required = true),
            @ApiImplicitParam(name = "productCode", value = "产品编码", paramType = "query", required = true),
            @ApiImplicitParam(name = "contractExpirationDate", value = "合约到期日", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierLevel", value = "供应商等级", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierAccountingPeriod", value = "供应商账期", paramType = "query", required = true),
            @ApiImplicitParam(name = "createSupplierRatingDate", value = "供应商评分", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierQualityEngineer", value = "供应商质量工程师", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext6", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext7", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext8", value = "扩展字段5", paramType = "query"),
            @ApiImplicitParam(name = "ext9", value = "扩展字段5", paramType = "query")
    })

    public Map<String, Object>updateSupplier(@RequestBody Map<String, String> paramMap) {

        return supplierService.updateSupplier(paramMap);
    }

    @PostMapping("/deleteSupplier")
    @ApiOperation(value = "删除供应商", notes = "删除供应商")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteSupplier(@RequestBody Map<String,String> paramMap) {
        return supplierService.deleteSupplier(paramMap);
    }
}
