package com.whjz.manage.controller.message;

import com.whjz.manage.service.impl.message.BondImpl;
import com.whjz.manage.service.message.BondService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author:hj
 * date:2020-10-6
 * remarks:供应商保证金类
 */
@Api(tags = "供应商保证金")
@Slf4j
@RestController
@RequestMapping("Bond")
public class BondController extends BondImpl {
    @Autowired
    @SuppressWarnings("all")
    private BondService bondService;

    @PostMapping("/queryBondList")
    @ApiOperation(value = "查询供应商保证金列表", notes = "根据条件查询供应商保证金返回供应商保证金列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondStandardCode", value = "保证金模板编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondState", value = "保证金状态", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentDate", value = "缴纳日期", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentUser", value = "缴纳人", paramType = "query", required = true),
            @ApiImplicitParam(name = "payee", value = "收款人", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentChannels", value = "支付渠道", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentVoucher", value = "支付凭证", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> queryBondList(@RequestBody Map<String, String> paramMap) {

        return bondService.queryBondList(paramMap);
    }
    @PostMapping("/insertBond")
    @ApiOperation(value = "添加供应商保证金列表", notes = "添加供应商保证金列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondStandardCode", value = "保证金模板编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondState", value = "保证金状态", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentDate", value = "缴纳日期", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentUser", value = "缴纳人", paramType = "query", required = true),
            @ApiImplicitParam(name = "payee", value = "收款人", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentChannels", value = "支付渠道", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentVoucher", value = "支付凭证", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> insertBond(@RequestBody Map<String, String> ParamMap) {
        return bondService.insertBond(ParamMap);
    }

    @PostMapping("/updateBond")
    @ApiOperation(value = "修改供应商保证金列表", notes = "修改供应商保证金列表")
    @ApiImplicitParams({
            //待填
            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondStandardCode", value = "保证金模板编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "bondState", value = "保证金状态", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentDate", value = "缴纳日期", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentUser", value = "缴纳人", paramType = "query", required = true),
            @ApiImplicitParam(name = "payee", value = "收款人", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentChannels", value = "支付渠道", paramType = "query", required = true),
            @ApiImplicitParam(name = "paymentVoucher", value = "支付凭证", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> updateBond(@RequestBody Map<String, String> ParamMap) {
        return bondService.updateBond(ParamMap);
    }

    @PostMapping("/deleteBond")
    @ApiOperation(value = "删除供应商保证金列表", notes = "删除供应商保证金列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteBond(@RequestBody Map<String,String> paramMap) {
        return bondService.deleteBond(paramMap);
    }

}
