package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.WaybillService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: hj
 * date: 2020-10-1
 * remarks：运单信息
 */
@Api(tags = {"运单信息"})
@RestController
@Slf4j
@RequestMapping("waybill")
public class WaybillController {

    @Autowired
    @SuppressWarnings("all")
    private WaybillService waybillService;

    @PostMapping("/queryWaybillList")
    @ApiOperation(value = "查询运单信息列表", notes = "根据条件运单信息返回运单信息列表")
    @ApiImplicitParams({

            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "waybillCode", value = "运单编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "供应商名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "waybillTypeName", value = "分类名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productSpecifications", value = "商品规格", paramType = "query", required = true),
            @ApiImplicitParam(name = "copiesNumber", value = "商品份数", paramType = "query"),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })

    public Map<String, Object> queryWaybillList(@RequestBody Map<String, String> paramMap) {

        return waybillService.queryWaybillList(paramMap);

    }
    @PostMapping("/insertWaybill")
    @ApiOperation(value = "添加运单信息列表", notes = "添加运单信息列表")
    @ApiImplicitParams({

            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "waybillCode", value = "运单编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "供应商名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "waybillTypeName", value = "分类名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productSpecifications", value = "商品规格", paramType = "query", required = true),
            @ApiImplicitParam(name = "copiesNumber", value = "商品份数", paramType = "query"),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> insertWaybill(@RequestBody Map<String, String> paramMap) {

        return waybillService.insertWaybill(paramMap);

    }
    @PostMapping("/updateWaybill")
    @ApiOperation(value = "修改运单信息列表", notes = "修改运单信息列表")
    @ApiImplicitParams({

            @ApiImplicitParam(name = "autoId", value = "序号", paramType = "query", required = true),
            @ApiImplicitParam(name = "waybillCode", value = "运单编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierCode", value = "供应商编号", paramType = "query", required = true),
            @ApiImplicitParam(name = "supplierName", value = "供应商名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "waybillTypeName", value = "分类名称", paramType = "query", required = true),
            @ApiImplicitParam(name = "productSpecifications", value = "商品规格", paramType = "query", required = true),
            @ApiImplicitParam(name = "copiesNumber", value = "商品份数", paramType = "query"),

            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query")
    })
    public Map<String, Object> updateWaybill(@RequestBody Map<String, String> paramMap) {

        return waybillService.updateWaybill(paramMap);

    }
    @PostMapping("/deleteWaybill")
    @ApiOperation(value = "删除运单信息列表", notes = "删除运单信息列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deleteWaybill(@RequestBody Map<String, String> paramMap) {
        return waybillService.deleteWaybill(paramMap);
    }

}
