package com.whjz.manage.controller.message;

import com.whjz.manage.service.message.PictureService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-10-1
 * remarks：图片相关接口
 */
@Api(tags = {"图片管理"})
@RestController
@Slf4j
@RequestMapping("picture")
public class PictureController extends AbstractControllerImpl {

    @Autowired
    private PictureService pictureService;

    @PostMapping("/queryPictureList")
    @ApiOperation(value = "查询图片列表", notes = "根据查询条件返回图片列表")
    @ApiImplicitParams({
           //待填
            @ApiImplicitParam(name = "pageNum", value = "当前页数", paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize", value = "查询记录数", paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage", value = "是否分页", paramType = "query", required = true),
    })
    public Map<String, Object> querypictureList(@RequestBody Map<String, String> paramMap) {
        return pictureService.queryPictureList(paramMap);
    }

    @PostMapping("/insertPicture")
    @ApiOperation(value = "添加图片", notes = "添加图片")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "channel", value = "渠道", paramType = "query"),
            @ApiImplicitParam(name = "frontSeqNo", value = "前端流水号", paramType = "query", required = true),
            @ApiImplicitParam(name = "backSeqNo", value = "后端流水号", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "照片类型 1-产品标题图2-栏目图标3-banner图4-评价晒图5-身份证正面 6-身份证反面 7-合同文件 8-首页新用户送菜图 9-个人头像 10-人脸识别照片 12-人脸识别照片 13-营业执照 根据实际情况待添加", paramType = "query"),
            @ApiImplicitParam(name = "fileType", value = "文件类型 1-图片 2-pdf", paramType = "query"),
            @ApiImplicitParam(name = "saveType", value = "1-临时储存 2-永久储存", paramType = "query"),
            @ApiImplicitParam(name = "address", value = "储存地址", paramType = "query", required = true),
            @ApiImplicitParam(name = "keyword", value = "储存关键字", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDeptId", value = "创建机构id", paramType = "query"),
            @ApiImplicitParam(name = "createDeptName", value = "创建机构名称", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query"),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> insertPicture(@RequestBody Map<String, String> paramMap) {
        return pictureService.insertPicture(paramMap);
    }

    @PostMapping("/updatePicture")
    @ApiOperation(value = "修改图片", notes = "修改图片")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId", value = "自增ID", paramType = "query"),
            @ApiImplicitParam(name = "channel", value = "渠道", paramType = "query"),
            @ApiImplicitParam(name = "frontSeqNo", value = "前端流水号", paramType = "query", required = true),
            @ApiImplicitParam(name = "backSeqNo", value = "后端流水号", paramType = "query"),
            @ApiImplicitParam(name = "type", value = "照片类型 1-产品标题图2-栏目图标3-banner图4-评价晒图5-身份证正面 6-身份证反面 7-合同文件 8-首页新用户送菜图 9-个人头像 10-人脸识别照片 12-人脸识别照片 13-营业执照 根据实际情况待添加", paramType = "query"),
            @ApiImplicitParam(name = "fileType", value = "文件类型 1-图片 2-pdf", paramType = "query"),
            @ApiImplicitParam(name = "saveType", value = "1-临时储存 2-永久储存", paramType = "query"),
            @ApiImplicitParam(name = "address", value = "储存地址", paramType = "query", required = true),
            @ApiImplicitParam(name = "keyword", value = "储存关键字", paramType = "query", required = true),
            @ApiImplicitParam(name = "createUser", value = "创建人", paramType = "query", required = true),
            @ApiImplicitParam(name = "createDeptId", value = "创建机构id", paramType = "query"),
            @ApiImplicitParam(name = "createDeptName", value = "创建机构名称", paramType = "query"),
            @ApiImplicitParam(name = "createDate", value = "创建时间", paramType = "query"),
            @ApiImplicitParam(name = "ext1", value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2", value = "扩展字段2", paramType = "query"),
            @ApiImplicitParam(name = "ext3", value = "扩展字段3", paramType = "query"),
            @ApiImplicitParam(name = "ext4", value = "扩展字段4", paramType = "query"),
            @ApiImplicitParam(name = "ext5", value = "扩展字段5", paramType = "query"),
    })
    public Map<String, Object> updatePicture(@RequestBody Map<String, String> paramMap) {
        return pictureService.updatePicture(paramMap);
    }

    @PostMapping("/deletePicture")
    @ApiOperation(value = "删除图片", notes = "删除图片")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds", value = "字符串拼接ID", paramType = "query", required = true),
    })
    public Map<String, Object> deletePicture(@RequestBody Map<String, String> paramMap) {
        return pictureService.deletePicture(paramMap);
    }
}
