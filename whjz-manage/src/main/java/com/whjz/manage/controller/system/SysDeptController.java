package com.whjz.manage.controller.system;

import com.whjz.manage.service.system.SysDeptService;
import com.whjz.service.impl.AbstractControllerImpl;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;

/**
 * author: fln
 * date: 2020-09-17
 * remarks：机构相关接口
 */

@Api(tags = {"机构管理"})
@RestController
@Slf4j
@RequestMapping("sysDept")
public class SysDeptController extends AbstractControllerImpl {

    @Autowired
    private SysDeptService sysDeptService;

    @PostMapping("/queryDeptList")
    @ApiOperation(value = "查询机构列表", notes = "根据查询条件返回机构列表")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deptCode",value = "机构编号  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "deptName",value = "机构名称  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "parentId",value = "上级机构编号  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "deptLevel",value = "机构级别  暂定四级0-公司 1-分公司 3.部门 4供应商  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "deptState",value = "机构状态 (1-启用  2-停用)  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "deptType",value = "机构类型 (1-供应商 2-分公司 3-菜鸟  4-物流)  筛选条件",paramType = "query"),
            @ApiImplicitParam(name = "pageNum",value = "当前页数",paramType = "query", required = true),
            @ApiImplicitParam(name = "pageSize",value = "查询记录数",paramType = "query", required = true),
            @ApiImplicitParam(name = "isPage",value = "是否分页",paramType = "query", required = true),
    })
    public Map<String,Object> queryDeptList(@RequestBody Map<String,String> paramMap) {
        return sysDeptService.queryDeptList(paramMap);
    }

    @PostMapping("/insertDept")
    @ApiOperation(value = "添加机构", notes = "添加机构")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "deptCode",value = "机构编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "deptName",value = "机构名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "parentDeptId",value = "上级机构ID",paramType = "query", required = true),
            @ApiImplicitParam(name = "shortName",value = "机构简称",paramType = "query"),
            @ApiImplicitParam(name = "deptLevel",value = "机构级别(暂定四级 0-公司 1-分公司 3.部门 4供应商)",paramType = "query", required = true),
            @ApiImplicitParam(name = "deptState",value = "机构状态(1-启用  2-停用)",paramType = "query", required = true),
            @ApiImplicitParam(name = "deptType",value = "机构类型",paramType = "query"),
            @ApiImplicitParam(name = "contUser",value = "负责人",paramType = "query", required = true),
            @ApiImplicitParam(name = "phonrNum",value = "联系电话",paramType = "query", required = true),
            @ApiImplicitParam(name = "address",value = "地址",paramType = "query", required = true),
            @ApiImplicitParam(name = "postCode",value = "邮编",paramType = "query"),
            @ApiImplicitParam(name = "email",value = "电子邮箱",paramType = "query"),
            @ApiImplicitParam(name = "remark",value = "备注",paramType = "query"),
            @ApiImplicitParam(name = "createUser",value = "创建人",paramType = "query", required = true),
            @ApiImplicitParam(name = "updateUser",value = "修改人",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
            @ApiImplicitParam(name = "ext6",value = "扩展字段6",paramType = "query"),
    })
    public Map<String,Object> insertDept(@RequestBody Map<String,String> paramMap) {
        return sysDeptService.insertDept(paramMap);
    }

    @PostMapping("/updateDept")
    @ApiOperation(value = "修改机构", notes = "修改机构")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoId",value = "自增Id",paramType = "query", required = true),
            @ApiImplicitParam(name = "deptCode",value = "机构编号",paramType = "query", required = true),
            @ApiImplicitParam(name = "deptName",value = "机构名称",paramType = "query", required = true),
            @ApiImplicitParam(name = "parentDeptId",value = "上级机构ID",paramType = "query", required = true),
            @ApiImplicitParam(name = "shortName",value = "机构简称",paramType = "query"),
            @ApiImplicitParam(name = "deptLevel",value = "机构级别(暂定四级 0-公司 1-分公司 3.部门 4供应商)",paramType = "query", required = true),
            @ApiImplicitParam(name = "deptState",value = "机构状态(1-启用  2-停用)",paramType = "query", required = true),
            @ApiImplicitParam(name = "deptType",value = "机构类型",paramType = "query"),
            @ApiImplicitParam(name = "contUser",value = "负责人",paramType = "query", required = true),
            @ApiImplicitParam(name = "phonrNum",value = "联系电话",paramType = "query", required = true),
            @ApiImplicitParam(name = "address",value = "地址",paramType = "query", required = true),
            @ApiImplicitParam(name = "postCode",value = "邮编",paramType = "query"),
            @ApiImplicitParam(name = "email",value = "电子邮箱",paramType = "query"),
            @ApiImplicitParam(name = "remark",value = "备注",paramType = "query"),
            @ApiImplicitParam(name = "createUser",value = "创建人",paramType = "query", required = true),
            @ApiImplicitParam(name = "updateUser",value = "修改人",paramType = "query", required = true),
            @ApiImplicitParam(name = "ext1",value = "扩展字段1", paramType = "query"),
            @ApiImplicitParam(name = "ext2",value = "扩展字段2",paramType = "query"),
            @ApiImplicitParam(name = "ext3",value = "扩展字段3",paramType = "query"),
            @ApiImplicitParam(name = "ext4",value = "扩展字段4",paramType = "query"),
            @ApiImplicitParam(name = "ext5",value = "扩展字段5",paramType = "query"),
            @ApiImplicitParam(name = "ext6",value = "扩展字段6",paramType = "query"),
    })
    public Map<String,Object> updateDept(@RequestBody Map<String,String> paramMap) {
        return sysDeptService.updateDept(paramMap);
    }

    @PostMapping("/deleteDept")
    @ApiOperation(value = "删除机构", notes = "删除机构")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "autoIds",value = "字符串拼接ID",paramType = "query", required = true),
    })
    public Map<String,Object> deleteDept(@RequestBody Map<String,String> paramMap) {
        return sysDeptService.deleteDept(paramMap);
    }
}
