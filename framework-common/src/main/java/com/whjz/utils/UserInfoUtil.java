package com.whjz.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import com.whjz.entity.MbBackUser;
import com.whjz.redis.RedisUtils;
import lombok.extern.slf4j.Slf4j;

/**
 * @auther czl
 * @date 2019/9/16 20:39
 */
@Slf4j
public class UserInfoUtil {
    /**
     * 获取登录用户基本信息
     *
     * @param userId
     * @param sourType 2-pad 3-后管
     * @return
     */
    public static MbBackUser getLocalUserInfo(String userId, String sourType){
        if("2".equals(sourType)){
            if(StringUtils.isNotNullOrEmtory(RedisUtils.getValue(StringConstantUtil.USER+userId))){
                JSONObject jsonObject=JSON.parseObject(RedisUtils.getValue(StringConstantUtil.USER+userId));
                MbBackUser userInfo=JSONObject.toJavaObject(jsonObject,MbBackUser.class);
                return userInfo;
            }
        }else {
            if(StringUtils.isNotNullOrEmtory(RedisUtils.getValue(StringConstantUtil.MANAGE_USER_ASSIST+userId))){
                JSONObject jsonObject=JSON.parseObject(RedisUtils.getValue(StringConstantUtil.MANAGE_USER_ASSIST+userId));
                MbBackUser userInfo=JSONObject.toJavaObject(jsonObject,MbBackUser.class);
                return userInfo;
            }
        }
        return null;
    }
    /**
     * 获取登录用户基本信息
     *
     * @param userId
     * @return
     */
    public static MbBackUser getLocalUserInfoWithOutSourceType(String userId){

            if(StringUtils.isNotNullOrEmtory(RedisUtils.getValue(StringConstantUtil.USER+userId))){
                JSONObject jsonObject=JSON.parseObject(RedisUtils.getValue(StringConstantUtil.USER+userId));
                MbBackUser userInfo=JSONObject.toJavaObject(jsonObject,MbBackUser.class);
                return userInfo;
            }else if(StringUtils.isNotNullOrEmtory(RedisUtils.getValue(StringConstantUtil.MANAGE_USER_ASSIST+userId))){
                JSONObject jsonObject=JSON.parseObject(RedisUtils.getValue(StringConstantUtil.MANAGE_USER_ASSIST+userId));
                MbBackUser userInfo=JSONObject.toJavaObject(jsonObject,MbBackUser.class);
                return userInfo;
            }

        return null;
    }
}
