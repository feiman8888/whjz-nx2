package com.whjz.utils;


import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import org.apache.commons.lang.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 分页工具
 * @author liweibin
 *
 */
public class PageUtil {

	/**
	 * 分页设置
	 * @author jlchen4
	 * @return
	 */
	public static Page startPage(Map<String, String> reqBody) {
		String pageSizeStr = reqBody.get("pageSize");		//每页数量
		String pageNumStr = reqBody.get("pageNum");			//页码
		int pageSize = 20;
		int pageNum = 1;

		//判断页数
		if(StringUtils.isNotBlank(pageSizeStr)) {
			pageSize = Integer.valueOf(pageSizeStr);
		}else{
            reqBody.put("pageSize",pageSize+"");
        }
		//判断页码
		if(StringUtils.isNotBlank(pageNumStr)) {
			pageNum = Integer.valueOf(pageNumStr);
		}else{
			reqBody.put("pageNum",pageNum+"");
		}
		Page o =	PageHelper.startPage(pageNum, pageSize);		//设置分页
		return o;
	}
	/**
	 * 分页设置
	 * @author jlchen4
	 * @return
	 */
	public static Integer getPageTotal(Map<String, String> reqBody,Integer totalCount) {
		String pageSizeStr = reqBody.get("pageSize");		//每页数量
		String pageNumStr = reqBody.get("pageNum");			//页码
		int pageSize = 20;
		int pageNum = 1;

		//判断页数
		if(StringUtils.isNotBlank(pageSizeStr)) {
			pageSize = Integer.valueOf(pageSizeStr);
		}
		//判断页码
		if(StringUtils.isNotBlank(pageNumStr)) {
			pageNum = Integer.valueOf(pageNumStr);
		}
		int num = totalCount / pageSize;
		int ccount = totalCount % pageSize;
		if(ccount > 0 ){
			num = num+1;
		}
		return num;
	}
	public static long getPageTotal(Map<String, String> reqBody,Long totalCount) {
		String pageSizeStr = reqBody.get("pageSize");		//每页数量
		String pageNumStr = reqBody.get("pageNum");			//页码
		int pageSize = 20;
		int pageNum = 1;

		//判断页数
		if(StringUtils.isNotBlank(pageSizeStr)) {
			pageSize = Integer.valueOf(pageSizeStr);
		}
		//判断页码
		if(StringUtils.isNotBlank(pageNumStr)) {
			pageNum = Integer.valueOf(pageNumStr);
		}
		long num = totalCount / pageSize;
		long ccount = totalCount % pageSize;
		if(ccount > 0 ){
			num = num+1;
		}
		return num;
	}
	/**
	  * @author ： lcc
	  *  @DEsc :获取分页标准返回
	  *  @date: 2019/5/6 20:37
	   * @Param: null
	  * @return
	  */
	public static Object getPageMap(Map<String, String> req,List data, Integer totalCount) {
		if(req.containsKey("isPage") && "true".equals(req.get("isPage"))) {
			String pageNumStr = req.get("pageNum");
			int pageNum = 1;
			if(StringUtils.isNotBlank(pageNumStr)) {
				pageNum = Integer.valueOf(pageNumStr);
			}
			Map map = new HashMap();
			Integer total = getPageTotal(req, totalCount);
			if(total < pageNum ){
				data = new ArrayList();
			}
			map.put("cList", data);
			map.put("total", total);
			map.put("totalNum",totalCount);
			return  map;
		}
		return data;

	}
	public static void getPageMap(Map in,Map out,List data, Integer totalCount,String dataNum) {
		if(in.containsKey("isPage") && "true".equals(in.get("isPage"))) {
			String pageNumStr = in.get("pageNum")+"";
			int pageNum = 1;
			if(StringUtils.isNotBlank(pageNumStr)) {
				pageNum = Integer.valueOf(pageNumStr);
			}
			Integer total = getPageTotal(in, totalCount);
			if(total < pageNum ){
				data = new ArrayList();
			}
			out.put("data"+dataNum, data);
			out.put("total"+dataNum, total);
			out.put("totalNum"+dataNum,totalCount);

		}


	}
	public static Map getPage(Map<String, String> req,List data, Integer totalCount) {
		Map map = new HashMap();
		map.put("cList", data);
		map.put("total", getPageTotal(req, totalCount));
		map.put("totalNum",totalCount);
		return  map;

	}
	public static Map getPage(Map<String, String> req,List data, Long totalCount) {
		Map map = new HashMap();
		map.put("cList", data);
		map.put("total", getPageTotal(req, totalCount));
		map.put("totalNum",totalCount);
		return  map;

	}



}
