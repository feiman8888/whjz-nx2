package com.whjz.utils;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>Title: EnumUtil</p>
 * <p>Description: 枚举</p>
 * @author xuzongtian
 * @date 2018年4月22日
 */
public class EnumUtil {

	/**
	 * <p>Title: ENUM_CACHE_TYPE</p>
	 * <p>Description: 缓存类型</p>
	 * @author xuzongtian
	 * @date 2018年4月22日
	 */
	public enum ENUM_CACHE_TYPE {
		VALUE_SEQUENCE("sequence", "序列");
		
		private String val;
		private String desc;
		
		private ENUM_CACHE_TYPE(String val, String desc) {
			this.val = val;
			this.desc = desc;
		}
		
		public String getVal() {
			return this.val;
		}
		
		public String getDesc() {
			return this.desc;
		}
		
		static Map<String, ENUM_CACHE_TYPE> values = new HashMap<String, ENUM_CACHE_TYPE>();
		static {
			values.put(VALUE_SEQUENCE.val, VALUE_SEQUENCE);
		}

		public static ENUM_CACHE_TYPE getENUM_CACHE_TYPE(String key) {
			return values.get(key);
		}

		public String getKey(String str) {
			return this.val + "_" + str;
		}
	}

	/**
	 * <p>Title: ENUM_SEQUENCE_TYPE</p>
	 * <p>Description: 序列类型</p>
	 * @author xuzongtian
	 * @date 2018年4月22日
	 */
	public enum ENUM_SEQUENCE_TYPE {
		VALUE_A("A", "A");

		private String val;
		private String desc;

		private ENUM_SEQUENCE_TYPE(String val, String desc) {
			this.val = val;
			this.desc = desc;
		}

		public String getVal() {
			return this.val;
		}

		public String getDesc() {
			return this.desc;
		}

		static Map<String, ENUM_SEQUENCE_TYPE> values = new HashMap<String, ENUM_SEQUENCE_TYPE>();
		static {
			values.put(VALUE_A.val, VALUE_A);
		}
		
		public static ENUM_SEQUENCE_TYPE getENUM_SEQUENCE_TYPE(String key) {
			return values.get(key);
		}
		
		public String getKey(String str) {
			return this.val + "_" + str;
		}
	}
	
}
