package com.whjz.utils;

/**
 * 错误编码类
 */
public enum CodeUtil {
    CODE_99999("其他异常", "99999"),
    CODE_10001("验证码错误", "10001"),
    CODE_10002("验证码发送错误", "10002"),
    CODE_10004("密码错误", "10004"),
    CODE_10000("参数错误", "10000"),
    CODE_10005("身份证不合法", "10005"),
    CODE_10007("首次登陆", "10007"),
    CODE_10008("密码为空", "10008"),
    CODE_10010("密码异常", "10010"),
    CODE_10011("用户不存在或状态不正常,请重新登录", "10011"),
    CODE_10016("验证码已过期", "10016"),
    CODE_10017("验证码输入错误", "10017"),
    CODE_10035("您输入的新密码和旧密码相同，请重新输入", "500"),
    CODE_2000("此数据已存在,不需要重复添加", "2000"),
    CODE_2001("更新数据已存在，不需要重复更新", "2001"),
    CODE_2002("删除数据与外键关联，不能删除", "2002"),
    CODE_601("baseInfo格式不对","601"),
    CODE_602("请勿重复提交","602"),
    CODE_404("不存在", "404"),
    CODE_500("错误", "500"),
    CODE_200("成功", "200"),
    CODE_3("用户已在新设备登录过，请确保是你本人操作", "3"),
    CODE_2("登录超时，请重新登录", "2"),
    CODE_1("你的账户正在其他设备登录，请确保是你本人操作", "1"),
    CODE_201("用户不存在", "201"),
    CODE_202("用户密码错误次数超过5次,请联系管理员解锁", "202"),
    CODE_203("您输入密码错误", "203"),
    CODE_204("用户密码超过180天未修改", "204"),
    CODE_205("用户密码需重置", "205"),
    CODE_206("数据库更新成功", "206"),
    CODE_207("数据库更新失败", "207"),
    CODE_208("短信模板1", "208"),
    CODE_213("合成图片失败", "213"),
    CODE_214("查询PDF资源失败", "214"),
    CODE_215("创建PDF失败", "215"),
    CODE_216("远程授权提交失败", "216"),
    CODE_10033("柜员号有误，请检查重新输入", "500"),
    CODE_10034("柜员信息有误，请联系管理员", "500"),
    CODE_4002("远程授权错误", "4002"),
    CODE_4003("远程授权柜员不在", "4003"),
    CODE_4004("查询无此申请记录,请重新申请授权", "4004"),
    CODE_4005("已分配未授权", "4005"),
    CODE_4006("已授权", "4006"),
    TRADITION_LOGIN("1", "传统登录"),
    FACE_LOGIN("3", "人脸登录"),
    BusStatus_1("待处理", "1"),
    BusStatus_2("成功", "2"),
    BusStatus_3("提交失败", "3"),//包含  ESB业务接口提交失败或者/远程授权提交失败)
    BusStatus_4("打回(提交成功,被打回的状态)", "4"),
    BusStatus_5("远程授权提交成功", "5"),
    BusStatus_6("远程授权成功", "6"),
    Bus_Is_Desplay_NO("业务统计中_不显示", "1"),
    Bus_Is_Desplay("业务统计中_显示", "2"),
    PasswodStatus_Reset("密码重置","1"),//默认密码状态
    PasswodStatus_Change("密码修改","2"),
    Login_Status_FristLogin("首次登录","3"),//,默认密码状态，180天未修改密码，跳转到修改密码界面
    Login_Status_Success("登录成功","2"),
    ;

    private String name;

    private String code;

    private CodeUtil(String name, String code) {
        this.name = name;
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public static CodeUtil findByCode(String code) {
        for (CodeUtil codeUtil : values()) {
            if (codeUtil.code.equals(code))
                return codeUtil;
        }
        return null;
    }

    public static void main(String[] args) {
        CodeUtil item = findByCode("301");
        System.out.print("item.code=" + item.name);
    }
}
