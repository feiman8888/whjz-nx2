package com.whjz.threadPool;


import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;

/**
 * 线程池
 *
 */
public class ThreadPool {

	private ThreadPoolExecutor pool = null;


	public ThreadPool(ThreadPoolConfig threadPoolConfig) {
		pool = new ThreadPoolExecutor(threadPoolConfig.getCorePoolSize(),
				threadPoolConfig.getMaximumPoolSize(),
				threadPoolConfig.getKeepAliveTime(),
				threadPoolConfig.getUnit(), threadPoolConfig.getWorkQueue());

	}

	public void execute(Runnable executor) {
		pool.execute(executor);
	}

	public Future<?> submit(Callable<?> callable) throws NullPointerException,
			RejectedExecutionException {
		Future<?> future = pool.submit(callable);
		return future;
	}

	public void shutDown() {
		pool.shutdown();
	}

	public boolean isTerminated() {
		return pool.isTerminated();
	}

/*	public void shotDownNow() {
		shotDownNow();
	}*/
}