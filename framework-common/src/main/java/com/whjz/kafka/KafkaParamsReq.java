package com.whjz.kafka;

import lombok.Data;

/**
 * kakfa请求参数
 * @author youkun
 * @data 2018/7/11
 */
@Data
public class KafkaParamsReq {

    /**
     * 参数类型
     */
    private Class typeClass;

    /**
     * 参数的值
     */
    private Object paramValue;

}
