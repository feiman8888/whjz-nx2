package com.whjz.workstation.controller;

import com.whjz.entity.MbBackUser;
import com.whjz.service.impl.AbstractControllerImpl;
import com.whjz.utils.*;
import com.whjz.workstation.service.CommonService;
import com.whjz.workstation.service.ManageUserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.Map;

/**
 * 公共模块
 *
 * @author liweibin
 */
@Api(tags = {"公共模块"})
@RestController
@RequestMapping("common")
@Slf4j
public class CommonController extends AbstractControllerImpl {

    @Autowired
    ManageUserService manageUserService;

    @Autowired
    CommonService commonService;


    @ApiOperation(value = "登录时发送短信接口", notes = "登录时发送短信接口)")
    @ResponseBody
    @ApiImplicitParams({
            @ApiImplicitParam(name = "noteTemplateType", value = "短信模板类型暂时默认送01", paramType = "query"),
            @ApiImplicitParam(name = "sourceType", value = "渠道类型 2-移动营销平台", paramType = "query"),
            @ApiImplicitParam(name = "custNo", value = "柜员号", paramType = "query"),
            @ApiImplicitParam(name = "busCode", value = "业务类型", paramType = "query", required = true),
    })
    @RequestMapping(value = "/sendNote", method = RequestMethod.POST)
    Map sendNoteByMaster(@RequestBody Map<String, String> req){
        checkNotNullOrEmpty(req, "custNo", "sourceType", "seqNo");
        log.info("=======>调用接口开始：日志流水号：" + req.get("logseq") + "======>前端参数是：" + JsonUtils.toJsonString(req));
//        @ApiImplicitParam(name = "MblNo",value = "手机号码",paramType = "query"), //发送短信接口需要手机号码
        //通过common查询用户信息得到手机号码
        Map paramMap = new HashMap();
        String custNo = StringUtils.getString(req.get("custNo"));
        paramMap.put("userId", custNo);
        MbBackUser mbBackUser = manageUserService.queryBankUserByUserId(paramMap);
        if (mbBackUser == null) {
            return ResultUtil.resultMap(CodeUtil.CODE_10033.getName(), ResultCode.RESUTL_CODE_FAILURE.code(), null);
        }
        String MblNo = mbBackUser.getPhone();
        if (org.apache.commons.lang3.StringUtils.isNotBlank(MblNo)) {
            req.put("orgId", mbBackUser.getOrgId());
            req.put("MblNo", MblNo);
            return commonService.sendNote(req);
        }
        return ResultUtil.resultMap(CodeUtil.CODE_10034.getName(), ResultCode.RESUTL_CODE_FAILURE.code(), null);
    }


    /**
     * 校验短信
     *
     * @param req
     * @return
     */
    @ApiOperation(value = "短信校验", notes = "短信校验")
    @ResponseBody
    @ApiImplicitParams({
            @ApiImplicitParam(name = "uuid", value = "短信校验码", paramType = "query"),
            @ApiImplicitParam(name = "noteNo", value = "短信随机数", paramType = "query")
    })
    @RequestMapping(value = "checkNote", method = RequestMethod.POST)
    Map<String, Object> checkNote(@RequestBody Map<String, String> req) {
        checkNotNullOrEmpty(req, "custNo", "sourceType", "seqNo");
        log.info("=======>调用接口开始：日志流水号：" + req.get("logseq") + "======>前端参数是：" + JsonUtils.toJsonString(req));
        return commonService.checkNote(req);
    }

}
