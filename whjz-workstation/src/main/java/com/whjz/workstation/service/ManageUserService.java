package com.whjz.workstation.service;

import com.whjz.entity.MbBackUser;

import java.util.Map;

public interface ManageUserService {

    MbBackUser queryBankUserByUserId(Map<String, String> req);

}
