package com.whjz.services;

import java.util.Map;

import com.whjz.entity.UserParam;

public interface UserService {
	
	//所有接口device验证
	public Map<String, Object> checkDevice(UserParam userParam);
	
	//登录接口token验证
	public Map<String, Object> checkToken(UserParam userParam);
	

}
